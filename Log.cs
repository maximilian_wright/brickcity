﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace BrickCity
{
    class Log
    {
        public const string LOG_DIR = "logs\\";
        public const string LOG_CRASH_DIR = "crashlogs\\";

        private static string LOG_FILE;

        public static bool running = false;
        private static Thread logThread;
        private static SafeList<object> messages;

        static Log()
        {
            if (!Directory.Exists(LOG_DIR))
                Directory.CreateDirectory(LOG_DIR);

            if (!Directory.Exists(LOG_CRASH_DIR))
                Directory.CreateDirectory(LOG_CRASH_DIR);

            messages = new SafeList<object>();

            logThread = new Thread(new ThreadStart(HandleLogging));
            logThread.Start();

            LOG_FILE = LOG_DIR + GetTimeFilename();
            WriteLine("Created log file: " + LOG_FILE);
        }

        public static void Close()
        {
            running = false;
            logThread.Abort();
        }

        public static void Write(object o)
        {
            messages.Add(o);
        }

        public static void WriteLine(object o)
        {
            Write(o);
            Write("\n");
        }

        public static void WriteErr(Exception e)
        {
            StreamWriter w = new StreamWriter(File.OpenWrite(LOG_CRASH_DIR + GetTimeFilename()));
            w.Write(e);
            w.Close();
        }

        private static string GetTimeFilename()
        {
            DateTime now = DateTime.Now;
            return now.Year + "_" + now.Month + "_" + now.Day + "-" + now.Hour + "_" + now.Minute + ".txt";
        }

        private static void WriteMessage(object o)
        {
            if (o != null)
                File.AppendAllText(LOG_FILE, o.ToString());
            else
                File.AppendAllText(LOG_FILE, "\n");
        }

        private static void HandleLogging()
        {
            running = true;

            while (running)
            {
                if (messages.Count > 0)
                {
                    WriteMessage(messages[0]);
                    messages.RemoveAt(0);
                }
            }
        }
    }
}
