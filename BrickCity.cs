﻿using BrickCity.Map;
using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace BrickCity
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class BrickCity : Game
    {
        public static BrickCity instance;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        public bool Debug { get; set; }
        public SpriteFont Font { get; private set; }

        private IState currentState;

        private Texture2D tex;

        public BrickCity()
        {
            instance = this;

            Exiting += Cleanup;

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = Room.WIDTH * Tile.SIZE;
            graphics.PreferredBackBufferHeight = Room.HEIGHT * Tile.SIZE;
            graphics.ApplyChanges();
            
            Window.Title = "BrickCity";
        }

        private void Cleanup(object sender, EventArgs e)
        {
            Log.Close();
        }

        public IState CurrentState
        {
            get { return currentState; }
            set
            {
                if (currentState != null)
                    Window.TextInput -= currentState.OnTextInput;

                currentState = value;
                Window.TextInput += currentState.OnTextInput;
            }
        }

        protected override void Initialize()
        {
            base.Initialize();

            IsMouseVisible = true;

            CurrentState = new StateMainMenu();
        }
        
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            Debug = false;
            Font = Content.Load<SpriteFont>("Fonts/anonymouspro-regular");

            TextureManager.instance.Init(Content);
            tex = TextureManager.instance.GetTexture("base");
        }

        protected override void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Input.Update();

            if (Input.GetKeyUp(Keys.F3))
                Debug = !Debug;

            CurrentState.Update(deltaTime);
        }
        
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.TransparentBlack);

            CurrentState.Render(spriteBatch);
            CurrentState.RenderGui(spriteBatch);

            if (Debug)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

                //mouse coords
                spriteBatch.Draw(tex, new Rectangle((int)Input.MousePosition.X, 0, 1, Window.ClientBounds.Height), Color.Green);
                spriteBatch.Draw(tex, new Rectangle(0, (int)Input.MousePosition.Y, Window.ClientBounds.Width, 1), Color.Green);

                int mod = Font.LineSpacing;
                int y = -mod;

                //mouse
                string mousePos = "Mouse: (" + Input.MousePosition.X + ", " + Input.MousePosition.Y + ")";
                Vector2 size = Font.MeasureString(mousePos);
                spriteBatch.DrawString(Font, mousePos, new Vector2(Window.ClientBounds.Width - size.X, y += mod), Color.Yellow);

                string mouseButtons = "Mouse Buttons: (Left=" + Input.GetMouse(0) + ", Right=" + Input.GetMouse(1) + ", Middle=" + Input.GetMouse(2) + ")";
                size = Font.MeasureString(mouseButtons);
                spriteBatch.DrawString(Font, mouseButtons, new Vector2(Window.ClientBounds.Width - size.X, y += mod), Color.Yellow);

                //gui
                y = Window.ClientBounds.Height;
                string[] keys = new string[] { "GUI", "Mouse", "Bounds", "TrueBounds" };
                Color[] colors = new Color[] { Color.Orange, Color.Green, Color.Blue, Color.Red };

                for (int i = 0; i < keys.Length; i++)
                {
                    size = Font.MeasureString(keys[i]);
                    spriteBatch.DrawString(Font, keys[i], new Vector2(Window.ClientBounds.Width - size.X, y -= mod), colors[i]);
                }

                spriteBatch.End();
            }
        }
    }
}
