﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BrickCity
{
    class TextureManager
    {
        public static TextureManager instance;

        private ContentManager content;
        private Dictionary<string, Texture2D> textures;

        static TextureManager()
        {
            if (instance == null)
                instance = new TextureManager();
        }

        private TextureManager()
        {
            textures = new Dictionary<string, Texture2D>();
        }

        public void Init(ContentManager content)
        {
            this.content = content;
        }

        public Texture2D GetTexture(string name)
        {
            if (!textures.ContainsKey(name))
                Load(name);

            return textures[name];
        }

        public Texture2D[] GetTextures(string[] names)
        {
            Texture2D[] results = new Texture2D[names.Length];
            int index = 0;

            foreach (string name in names)
                results[index++] = GetTexture(name);

            return results;
        }

        public Texture2D GetTile(int id)
        {
            if (!textures.ContainsKey("Tiles/" + id))
                Load("Tiles/" + id);

            return textures["Tiles/" + id];
        }

        public Texture2D GetBoss(string name)
        {
            if (!textures.ContainsKey("Bosses/" + name))
                Load("Bosses/" + name);

            return textures["Bosses/" + name];
        }

        public Texture2D[] GetBossTextures(string[] names)
        {
            Texture2D[] results = new Texture2D[names.Length];
            int index = 0;

            foreach (string name in names)
                results[index++] = GetTexture("Bosses/" + name);

            return results;
        }

        private void Load(string texture)
        {
            textures.Add(texture, content.Load<Texture2D>(texture));
        }
    }
}
