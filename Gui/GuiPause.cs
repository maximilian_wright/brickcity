﻿using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace BrickCity.Gui
{
    class GuiPause : Gui
    {
        public const string NAME = "Pause";

        private GuiButton resumeButton;
        private GuiButton optionsButton;
        private GuiButton exitMenuButton;
        private GuiButton exitButton;

        public GuiPause(IState state, bool vis = true) : base(state, NAME, vis)
        {
            Rectangle size = BrickCity.instance.Window.ClientBounds;

            GuiLabel title = new GuiLabel(this, "Paused", 0, 10, EAlignment.CENTER, EAlignment.CENTER);

            int mod = 60;
            int y = size.Y / 3 - mod;

            resumeButton = new GuiButton(this, "Resume", 0, y += mod, 200, 50);
            optionsButton = new GuiButton(this, "Options", 0, y += mod, 200, 50);
            exitMenuButton = new GuiButton(this, "Exit to Menu", 0, y += mod, 200, 50);
            exitButton = new GuiButton(this, "Exit", 0, y += mod, 200, 50);

            resumeButton.Clicked += (mx, my, button) =>
            {
                Log.WriteLine("Resume clicked");
                state.HideGui(NAME);
            };
            optionsButton.Clicked += (mx, my, button) =>
            {
                Log.WriteLine("Options clicked");
                //state.ShowGui(GuiOptions.NAME);
                //state.HideGui(NAME);
            };
            exitMenuButton.Clicked += (mx, my, button) =>
            {
                Log.WriteLine("Exit to menu clicked");
                BrickCity.instance.CurrentState = new StateMainMenu();
            };
            exitButton.Clicked += (mx, my, button) =>
            {
                Log.WriteLine("Exit to desktop clicked");
                BrickCity.instance.Exit();
            };

            AddComponent(title);

            AddComponent(resumeButton);
            AddComponent(optionsButton);
            AddComponent(exitMenuButton);
            AddComponent(exitButton);

            X = size.Width / 2 - Width / 2;
            Y = size.Height / 2 - Height / 2;
            title.X = resumeButton.X = optionsButton.X = exitMenuButton.X = exitButton.X = Width / 2;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            if (Input.GetKeyUp(Keys.Escape))
                State.ShowGui(NAME);
        }
    }
}
