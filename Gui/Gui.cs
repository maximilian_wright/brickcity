﻿using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BrickCity.Gui
{
    class Gui : IGui
    {
        public string Name { get; private set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        
        public IState State { get; private set; }

        public bool Visible { get; private set; }
        protected List<IGuiComponent> components;

        protected int border = 3;
        protected Texture2D topleft;
        protected Texture2D left;
        protected Texture2D center;

        public Gui(IState state, string name, bool vis = true)
        {
            Name = name;
            Visible = vis;
            Width = Height = 0;

            State = state;

            components = new List<IGuiComponent>();
            topleft = TextureManager.instance.GetTexture("Gui/topleft");
            left = TextureManager.instance.GetTexture("Gui/left");
            center = TextureManager.instance.GetTexture("Gui/center");
        }

        public Rectangle Bounds
        {
            get { return new Rectangle(X, Y, Width, Height); }
        }

        public Vector2 Position
        {
            get { return new Vector2(X, Y); }
            set { X = (int)value.X; Y = (int)value.Y; }
        }

        public void AddComponent(IGuiComponent igc)
        {
            components.Add(igc);

            CalculateBounds();
        }

        public void RemoveComponent(IGuiComponent igc)
        {
            components.Remove(igc);

            CalculateBounds();
        }

        public void RemoveComponentAt(int index)
        {
            components.RemoveAt(index);

            CalculateBounds();
        }

        public void RemoveAllComponents()
        {
            components.Clear();

            Width = Height = 0;
        }

        protected virtual void CalculateBounds()
        {
            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = Vector2.Zero;

            foreach (IGuiComponent i in components)
            {
                Rectangle b = i.Bounds;

                if (b.X < min.X) min.X = b.X;
                if (b.Y < min.Y) min.Y = b.Y;

                if (b.Right > max.X) max.X = b.Right;
                if (b.Bottom > max.Y) max.Y = b.Bottom;
            }

            min.X -= border; min.Y -= border;
            max.X += border; max.Y += border;

            Width = (int)(max.X - min.X);
            Height = (int)(max.Y - min.Y);
        }

        public bool IsVisible
        {
            get { return Visible; }
        }

        public virtual void Show()
        {
            Visible = true;
        }

        public virtual void Hide()
        {
            Visible = false;
        }

        public virtual void Update(float deltaTime)
        {
            if (!Visible) return;

            foreach (IGuiComponent igc in components)
                igc.Update(deltaTime);
        }

        public virtual void Render(SpriteBatch spriteBatch)
        {
            if (!Visible) return;

            RenderBackground(spriteBatch);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null, Matrix.CreateTranslation(X, Y, 0));

            foreach (IGuiComponent igc in components)
            {
                igc.Render(spriteBatch);

                if (BrickCity.instance.Debug)
                    DrawRect(spriteBatch, igc.Bounds, Color.Blue);
            }

            spriteBatch.End();

            if (BrickCity.instance.Debug)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

                DrawRect(spriteBatch, Bounds, Color.Orange);

                foreach (IGuiComponent igc in components)
                    DrawRect(spriteBatch, igc.TrueBounds, Color.Red);

                spriteBatch.End();
            }
        }

        protected virtual void RenderBackground(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null, null);
            spriteBatch.Draw(center, Bounds, Color.White);
            spriteBatch.End();

            Rectangle corner = new Rectangle(0, 0, 3, 3);
            
            //top left
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateTranslation(X, Y, 0));
            spriteBatch.Draw(topleft, corner, Color.White);
            spriteBatch.End();

            //top right
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateRotationZ(MathHelper.ToRadians(90)) * Matrix.CreateTranslation(X + Width, Y, 0));
            spriteBatch.Draw(topleft, corner, Color.White);
            spriteBatch.End();

            //bottom left
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateRotationZ(MathHelper.ToRadians(270)) * Matrix.CreateTranslation(X, Y + Height, 0));
            spriteBatch.Draw(topleft, corner, Color.White);
            spriteBatch.End();

            //bottom right
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateRotationZ(MathHelper.ToRadians(180)) * Matrix.CreateTranslation(X + Width, Y + Height, 0));
            spriteBatch.Draw(topleft, corner, Color.White);
            spriteBatch.End();
            
            //left
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateTranslation(X, Y, 0));
            spriteBatch.Draw(left, new Rectangle(0, 3, 3, Height - 6), Color.White);
            spriteBatch.End();
            
            //right
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateRotationZ(MathHelper.ToRadians(180)) * Matrix.CreateTranslation(X + Width, Y + Height, 0));
            spriteBatch.Draw(left, new Rectangle(0, 3, 3, Height - 6), Color.White);
            spriteBatch.End();
            
            //top
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateRotationZ(MathHelper.ToRadians(90)) * Matrix.CreateTranslation(X + Width - 3, Y, 0));
            spriteBatch.Draw(left, new Rectangle(0, 0, 3, Width - 6), Color.White);
            spriteBatch.End();
            
            //bottom
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null,
                Matrix.CreateRotationZ(MathHelper.ToRadians(270)) * Matrix.CreateTranslation(X + 3, Y + Height, 0));
            spriteBatch.Draw(left, new Rectangle(0, 0, 3, Width - 6), Color.White);
            spriteBatch.End();
        }

        private void DrawRect(SpriteBatch spriteBatch, Rectangle rect, Color color)
        {
            Texture2D b = TextureManager.instance.GetTexture("base");
            spriteBatch.Draw(b, new Rectangle(rect.X, rect.Y, rect.Width, 1), color);
            spriteBatch.Draw(b, new Rectangle(rect.X, rect.Bottom - 1, rect.Width, 1), color);
            spriteBatch.Draw(b, new Rectangle(rect.X, rect.Y, 1, rect.Height), color);
            spriteBatch.Draw(b, new Rectangle(rect.Right - 1, rect.Y, 1, rect.Height), color);
        }

        public virtual void OnTextInput(object sender, TextInputEventArgs tiea)
        {
            foreach (IGuiComponent igc in components)
                igc.OnTextInput(sender, tiea);
        }
    }
}
