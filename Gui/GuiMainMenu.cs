﻿using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace BrickCity.Gui
{
    class GuiMainMenu : Gui
    {
        public const string NAME = "MainMenu";

        private GuiButton startButton;
        private GuiButton optionsButton;
        private GuiButton exitButton;

        private int index = 0;
        private bool mouseControl = false;
        private bool prevMouseControl = false;
        private bool buttonControl = false;

        public GuiMainMenu(IState state, bool vis = true) : base(state, NAME, vis)
        {
            Rectangle size = BrickCity.instance.Window.ClientBounds;

            GuiLabel title = new GuiLabel(this, "Brick City", 0, 10, EAlignment.CENTER, EAlignment.CENTER);

            int mod = 60;
            int y = size.Y / 3 - mod;

            startButton = new GuiButton(this, "Play", 0, y += mod, 200, 50);
            optionsButton = new GuiButton(this, "Options", 0, y += mod, 200, 50);
            exitButton = new GuiButton(this, "Exit", 0, y += mod, 200, 50);

            startButton.Clicked += (mx, my, button) =>
            {
                Log.WriteLine("Play clicked");
                state.ShowGui(GuiChar.NAME);
                state.HideGui(NAME);
            };
            optionsButton.Clicked += (mx, my, button) =>
            {
                Log.WriteLine("Options clicked");
                //state.ShowGui(GuiOptions.NAME);
                //state.HideGui(NAME);
            };
            exitButton.Clicked += (mx, my, button) =>
            {
                BrickCity.instance.Exit();
            };

            AddComponent(title);

            AddComponent(startButton);
            AddComponent(optionsButton);
            AddComponent(exitButton);

            X = size.Width / 2 - Width / 2;
            Y = size.Height / 2 - Height / 2;
            title.X = startButton.X = optionsButton.X = exitButton.X = Width / 2;

            SetButtonStates(1);
        }

        public override void Update(float deltaTime)
        {
            if (!Visible) return;

            base.Update(deltaTime);

            prevMouseControl = mouseControl;
            mouseControl = Input.DidMouseMove() || startButton.Hovered || optionsButton.Hovered || exitButton.Hovered;

            //detect switch between controls
            if (!mouseControl)
            {
                if (Input.GetKeyUp(Keys.Up) || Input.GetKeyUp(Keys.Down) || Input.GetKeyUp(Keys.Space))
                {
                    if (!buttonControl)
                    {
                        buttonControl = true;
                        SetButtonStates(1);
                    }
                    else
                    {
                        if (Input.GetKeyUp(Keys.Up))
                        {
                            SetButtonStates(0);
                            index = (index + 2) % 3;
                            SetButtonStates(1);
                        }
                        else if (Input.GetKeyUp(Keys.Down))
                        {
                            SetButtonStates(0);
                            index = (index + 1) % 3;
                            SetButtonStates(1);
                        }
                        else if (Input.GetKeyUp(Keys.Space))
                        {
                            SetButtonStates(2);

                            switch (index)
                            {
                                case 0:
                                    startButton.Click();
                                    break;
                                case 1:
                                    optionsButton.Click();
                                    break;
                                case 2:
                                    exitButton.Click();
                                    break;
                            }
                        }
                    }
                }
                else if (prevMouseControl)
                {
                    mouseControl = true;
                    buttonControl = false;
                }
                else if (!prevMouseControl)
                {
                    mouseControl = startButton.Hovered || optionsButton.Hovered || exitButton.Hovered;
                    buttonControl = !mouseControl;
                }
            }

            if (mouseControl && !prevMouseControl)
                SetButtonStates(0);
        }

        private void SetButtonStates(int state)
        {
            switch (index)
            {
                case 0:
                    startButton.SetButtonState(state);
                    break;
                case 1:
                    optionsButton.SetButtonState(state);
                    break;
                case 2:
                    exitButton.SetButtonState(state);
                    break;
            }
        }
    }
}
