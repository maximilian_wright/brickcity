﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Gui
{
    public interface IGuiComponent
    {
        bool Enabled { get; set; }
        int X { get; set; }
        int Y { get; set; }
        int Width { get; set; }
        int Height { get; set; }
        Rectangle Bounds { get; }
        Vector2 Position { get; set; }
        Rectangle TrueBounds { get; }

        IGui Gui { get; }

        void Update(float deltaTime);
        void Render(SpriteBatch spriteBatch);

        void OnTextInput(object sender, TextInputEventArgs tiea);
    }
}
