﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BrickCity.Gui
{
    class GuiTextField : GuiLabel
    {
        public delegate void OnSubmit(string text);
        public event OnSubmit Submit;

        public float BlinkRate { get; set; }
        private float BlinkTime { get { return 1 / BlinkRate; } }
        public int CharLimit { get; set; }
        public bool ShowCursor { get; set; }

        private enum EFieldState { NORMAL, HOVER, FOCUSED }
        private EFieldState fieldState = EFieldState.NORMAL;

        private Texture2D[] textures;
        private Texture2D textCursor;

        private static char[] CHARS = new char[] {
            'a', 'A',
            'b', 'B',
            'c', 'C',
            'd', 'D',
            'e', 'E',
            'f', 'F',
            'g', 'G',
            'h', 'H',
            'i', 'I',
            'j', 'J',
            'k', 'K',
            'l', 'L',
            'm', 'M',
            'n', 'N',
            'o', 'O',
            'p', 'P',
            'q', 'Q',
            'r', 'R',
            's', 'S',
            't', 'T',
            'u', 'U',
            'v', 'V',
            'w', 'W',
            'x', 'X',
            'y', 'Y',
            'z', 'Z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
        };
        private HashSet<char> acceptedCharacters;

        private float blinkTimer = 0;
        private bool cursorOn = true;

        public GuiTextField(IGui gui, Rectangle rect) : this(gui, rect.X, rect.Y, rect.Width, rect.Height) { }
        public GuiTextField(IGui gui, int x, int y, int w, int h) : base(gui, "", x, y, EAlignment.LEFT, EAlignment.CENTER)
        {
            Width = w;
            Height = h;

            BlinkRate = 1.5f;
            CharLimit = 20;
            ShowCursor = true;

            CreateAccepted();

            textures = new Texture2D[3];
            textures[0] = TextureManager.instance.GetTexture("Gui/textfield");
            textures[1] = TextureManager.instance.GetTexture("Gui/textfieldHover");
            textures[2] = TextureManager.instance.GetTexture("Gui/textfieldFocused");
            textCursor = TextureManager.instance.GetTexture("base");
        }

        public override string Text
        {
            get { return text.ToString(); }
            set
            {
                text.Clear();
                text.Append(value);
            }
        }

        public override Rectangle TrueBounds
        {
            get
            {
                Rectangle bounds = new Rectangle(Gui.X + X, Gui.Y + Y, Width, Height);

                switch (HAlignment)
                {
                    case EAlignment.CENTER:
                        bounds.X -= Width / 2;
                        break;
                    case EAlignment.RIGHT:
                        bounds.X -= Width;
                        break;
                }
                switch (VAlignment)
                {
                    case EAlignment.CENTER:
                        bounds.Y -= Height / 2;
                        break;
                    case EAlignment.BOTTOM:
                        bounds.Y -= Height;
                        break;
                }

                return bounds;
            }
        }

        private void CreateAccepted()
        {
            acceptedCharacters = new HashSet<char>();

            foreach (char c in CHARS)
                acceptedCharacters.Add(c);
        }

        public override void Update(float deltaTime)
        {
            if (!Enabled) return;

            blinkTimer += deltaTime;

            if (blinkTimer >= BlinkTime)
            {
                blinkTimer = 0;
                cursorOn = !cursorOn;
            }

            switch (fieldState)
            {
                case EFieldState.FOCUSED:
                    if (Input.GetMouseUp(0))
                        if (!TrueBounds.Contains(Input.MousePosition))
                            fieldState = EFieldState.NORMAL;
                    break;
                default:
                    if (TrueBounds.Contains(Input.MousePosition))
                    {
                        fieldState = EFieldState.HOVER;

                        if (Input.GetMouseUp(0))
                            fieldState = EFieldState.FOCUSED;
                    }
                    else
                        fieldState = EFieldState.NORMAL;
                    break;
            }
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Enabled) return;

            Vector2 mod = Vector2.Zero;

            switch (HAlignment)
            {
                case EAlignment.CENTER:
                    mod.X = -Width / 2;
                    break;
                case EAlignment.RIGHT:
                    mod.X = -Width;
                    break;
            }
            switch (VAlignment)
            {
                case EAlignment.CENTER:
                    mod.Y = -Height / 2;
                    break;
                case EAlignment.BOTTOM:
                    mod.Y = -Height;
                    break;
            }

            spriteBatch.Draw(textures[(int)fieldState], new Rectangle((int)(X + mod.X), (int)(Y + mod.Y), Width, Height), Color.White);

            if (Text.Length > 0)
                base.Render(spriteBatch);

            if (Text.Length <= CharLimit && fieldState == EFieldState.FOCUSED && cursorOn)
            {
                Vector2 len = Vector2.Zero;

                if (Text.Length == 0)
                {
                    len = Font.MeasureString("|");
                    len.Y += mod.Y;
                }
                else
                {
                    len = Font.MeasureString(Text);
                    len += mod;
                }

                spriteBatch.Draw(textCursor, new Rectangle((int)(X + len.X), (int)(Y + len.Y), 3, Font.LineSpacing), Color);
            }
        }

        public override void OnTextInput(object sender, TextInputEventArgs tiea)
        {
            if (fieldState != EFieldState.FOCUSED) return;
            
            switch (tiea.Character)
            {
                case '\b':
                    if (Text.Length > 0)
                        Text = Text.Remove(Text.Length - 1, 1);
                    break;
                case '\n':
                case '\r':
                    Submit?.Invoke(Text.ToString());
                    fieldState = EFieldState.NORMAL;
                    break;
                case '\t':
                    break;
                default:
                    if (Text.Length < CharLimit && acceptedCharacters.Contains(tiea.Character))
                        Text += tiea.Character;
                    break;
            }
        }
    }
}
