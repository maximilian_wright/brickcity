﻿using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BrickCity.Gui
{
    class GuiNotification : Gui
    {
        public const string NAME = "Notification";

        private GuiLabel message;

        private enum EState { NONE, UP, WAIT, DOWN }
        private EState state = EState.NONE;

        private bool animating = false;
        private float slideTime = 1.2f;
        private float slideTimer = 0;
        private int mod;

        private int min;
        private int max;

        private List<string> notifications = new List<string>();

        public GuiNotification(IState state, string text = "", bool vis = true) : base(state, NAME, vis)
        {
            message = new GuiLabel(this, text, 0, 0, EAlignment.LEFT, EAlignment.CENTER);

            AddComponent(message);
        }

        public void Notify(string text)
        {
            notifications.Add(text);
        }

        protected override void CalculateBounds()
        {
            base.CalculateBounds();
            
            Rectangle windowSize = BrickCity.instance.Window.ClientBounds;
            X = windowSize.Width - windowSize.Width / 20 - Width;
            min = windowSize.Height - windowSize.Height / 20 - Height;
            max = windowSize.Height + 10;
            Y = max;

            message.Y = Height / 4;
        }

        private void SlideUp()
        {
            state = EState.UP;
            animating = true;
            slideTimer = 0;

            mod = -(int)((max - min) / slideTime);
        }

        private void SlideDown()
        {
            state = EState.DOWN;
            animating = true;
            slideTimer = 0;

            mod = (int)((max - min) / slideTime);
        }

        public override void Update(float deltaTime)
        {
            if (!Visible) return;

            base.Update(deltaTime);
            
            if (!animating && notifications.Count > 0)
            {
                message.Text = notifications[0];
                notifications.RemoveAt(0);
                CalculateBounds();
                SlideUp();
            }
            else
            {
                slideTimer += deltaTime;

                switch (state)
                {
                    case EState.UP:
                        if (slideTimer >= slideTime)
                        {
                            state = EState.WAIT;
                            slideTimer = 0;
                        }

                        Y = Clamp(Y += mod, min, max);
                        break;
                    case EState.WAIT:
                        if (slideTimer >= slideTime)
                            SlideDown();
                        break;
                    case EState.DOWN:
                        if (slideTimer >= slideTime)
                        {
                            state = EState.NONE;
                            animating = false;
                        }

                        Y = Clamp(Y += mod, min, max);
                        break;
                }
            }
        }

        private int Clamp(int value, int min, int max)
        {
            if (value < min) return min;
            else if (value > max) return max;

            return value;
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Visible || !animating) return;

            RenderBackground(spriteBatch);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null, Matrix.CreateTranslation(X + border, Y + border, 0));

            foreach (IGuiComponent igc in components)
                igc.Render(spriteBatch);

            spriteBatch.End();
        }
    }
}
