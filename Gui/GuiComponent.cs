﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Gui
{
    class GuiComponent : IGuiComponent
    {
        public bool Enabled { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public IGui Gui { get; private set; }

        public GuiComponent(IGui gui, int x, int y)
        {
            Enabled = true;
            X = x;
            Y = y;

            Gui = gui;
        }

        public Rectangle Bounds
        {
            get { return new Rectangle(X, Y, Width, Height); }
        }

        public virtual Rectangle TrueBounds
        {
            get { return new Rectangle(Gui.X + X, Gui.Y + Y, Width, Height); }
        }

        public Vector2 Position
        {
            get { return new Vector2(X, Y); }
            set { X = (int)value.X; Y = (int)value.Y; }
        }

        public virtual void Update(float deltaTime)
        {
            if (!Enabled) return;
        }

        public virtual void Render(SpriteBatch spriteBatch)
        {
            if (!Enabled) return;
        }

        public virtual void OnTextInput(object sender, TextInputEventArgs tiea)
        {
            if (!Enabled) return;
        }
    }
}
