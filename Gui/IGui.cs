﻿using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Gui
{
    public interface IGui
    {
        string Name { get; }
        int X { get; set; }
        int Y { get; set; }
        int Width { get; }
        int Height { get; }
        Rectangle Bounds { get; }
        Vector2 Position { get; set; }

        IState State { get; }

        void AddComponent(IGuiComponent igc);
        void RemoveComponent(IGuiComponent igc);
        void RemoveComponentAt(int index);
        void RemoveAllComponents();
        
        bool IsVisible { get; }
        void Show();
        void Hide();

        void Update(float deltaTime);
        void Render(SpriteBatch spriteBatch);

        void OnTextInput(object sender, TextInputEventArgs tiea);
    }
}
