﻿using BrickCity.Entities;
using BrickCity.State;
using Microsoft.Xna.Framework;

namespace BrickCity.Gui
{
    class GuiGameOver : Gui
    {
        public const string NAME = "GameOver";

        private Player Player { get; set; }

        public GuiGameOver(IState state, Player player, bool vis = true) : base(state, NAME, vis)
        {
            Player = player;

            Rectangle size = BrickCity.instance.Window.ClientBounds;

            GuiLabel title = new GuiLabel(this, "GAME OVER", 0, 10, EAlignment.CENTER, EAlignment.CENTER);
            GuiLabel name = new GuiLabel(this, "Name: " + Player.Name, 20, 30, EAlignment.LEFT, EAlignment.CENTER);

            GuiLabel health = new GuiLabel(this, "Health: " + Player.MaxHealth, 20, 50, EAlignment.LEFT, EAlignment.CENTER);
            GuiLabel damage = new GuiLabel(this, "Damage: " + Player.BulletDamage, 20, 70, EAlignment.LEFT, EAlignment.CENTER);
            GuiLabel bsize = new GuiLabel(this, "Size: " + Player.BulletSize, 20, 90, EAlignment.LEFT, EAlignment.CENTER);

            GuiLabel message = new GuiLabel(this, "Press SPACE to continue...", 0, 115, EAlignment.CENTER, EAlignment.BOTTOM);

            AddComponent(title);
            AddComponent(name);
            AddComponent(health);
            AddComponent(damage);
            AddComponent(bsize);
            AddComponent(message);

            X = size.Width / 2 - Width / 2;
            Y = size.Height / 2 - Height / 2;

            title.X = Width / 2;
            message.X = Width / 2;
        }

        protected override void CalculateBounds()
        {
            base.CalculateBounds();

            //add border on bottom edge
            Height += 5;
        }
    }
}
