﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Gui
{
    class GuiTexture : GuiComponent
    {
        public Texture2D Texture { get; set; }
        public Color Color { get; set; }

        public GuiTexture(IGui gui, int x, int y, string texture) : this(gui, x, y, TextureManager.instance.GetTexture(texture)) { }
        public GuiTexture(IGui gui, int x, int y, Texture2D texture) : base(gui, x, y)
        {
            Texture = texture;
            Color = Color.White;
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Enabled) return;

            spriteBatch.Draw(Texture, Bounds, Color);
        }
    }
}
