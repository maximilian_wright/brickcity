﻿using BrickCity.Map;
using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace BrickCity.Gui
{
    class GuiMap : Gui
    {
        public const string NAME = "Map";

        private Floor floor;

        private Texture2D texture;
        private int roomBorder = 8;
        private int borderHalf;

        //offset x/y
        private int ox;
        private int oy;

        public GuiMap(IState state, Floor floor, bool vis = true) : base(state, NAME, vis)
        {
            Floor = floor;

            texture = TextureManager.instance.GetTexture("base");
            borderHalf = roomBorder / 2;
        }

        public Floor Floor
        {
            get { return floor; }
            set
            {
                floor = value;
                CalculateBounds();
            }
        }

        protected override void CalculateBounds()
        {
            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = Vector2.Zero;

            Dictionary<Tuple<int, int>, Room> rooms = Floor.Rooms;
            Dictionary<Tuple<int, int>, Room>.KeyCollection keys = rooms.Keys;

            foreach (Tuple<int, int> key in keys)
            {
                if (key.Item1 < min.X) min.X = key.Item1;
                if (key.Item2 < min.Y) min.Y = key.Item2;

                if (key.Item1 > max.X) max.X = key.Item1;
                if (key.Item2 > max.Y) max.Y = key.Item2;
            }

            ox = -(int)min.X;
            oy = -(int)min.Y;

            int roomsWide = (int)(Math.Abs(max.X - min.X) + 1);
            int roomsTall = (int)(Math.Abs(max.Y - min.Y) + 1);

            Width = roomsWide * (16 + roomBorder);
            Height = roomsTall * (16 + roomBorder);

            Rectangle windowSize = BrickCity.instance.Window.ClientBounds;

            X = windowSize.Width - windowSize.Width / 20 - Width;
            Y = windowSize.Width / 20;
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Visible) return;

            RenderBackground(spriteBatch);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null, Matrix.CreateTranslation(X + ox * (16 + roomBorder) + border, Y + oy * (16 + roomBorder) + border, 0));

            //draw map
            Dictionary<Tuple<int, int>, Room> rooms = Floor.Rooms;
            Dictionary<Tuple<int, int>, Room>.KeyCollection keys = rooms.Keys;

            foreach (Tuple<int, int> key in keys)
            {
                if (!rooms[key].Discovered) continue;

                Color color = rooms[key] == Floor.CurrentRoom ? Color.Red : Color.Orange;
                EDirection exits = rooms[key].Exits;

                if (rooms[key] is TreasureRoom)
                    color = Color.White;
                else if (rooms[key] is BossRoom)
                    color = Color.Blue;

                int x = key.Item1 * (16 + roomBorder);
                int y = key.Item2 * (16 + roomBorder);

                spriteBatch.Draw(texture, new Rectangle(x, y, 16, 16), color);

                if (exits.HasFlag(EDirection.DOWN))
                    spriteBatch.Draw(texture, new Rectangle(x + 7, y + 16, 2, borderHalf), color);
                if (exits.HasFlag(EDirection.LEFT))
                    spriteBatch.Draw(texture, new Rectangle(x - borderHalf, y + 7, borderHalf, 2), color);
                if (exits.HasFlag(EDirection.UP))
                    spriteBatch.Draw(texture, new Rectangle(x + 7, y - borderHalf, 2, borderHalf), color);
                if (exits.HasFlag(EDirection.RIGHT))
                    spriteBatch.Draw(texture, new Rectangle(x + 16, y + 7, borderHalf, 2), color);
            }

            spriteBatch.End();
        }
    }
}
