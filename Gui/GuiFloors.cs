﻿using BrickCity.Map;
using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Gui
{
    class GuiFloors : Gui
    {
        public const string NAME = "Floors";

        private Floor floor;

        private int lastLevel;

        private Texture2D locked;
        private Texture2D on;
        private Texture2D complete;
        private Texture2D[] badges;

        public GuiFloors(IState state, Floor floor, bool vis = true) : base(state, NAME, vis)
        {
            on = TextureManager.instance.GetTexture("floor0");
            complete = TextureManager.instance.GetTexture("floor1");
            locked = TextureManager.instance.GetTexture("floor2");
            badges = new Texture2D[8];

            for (int i = 0; i < 8; i++)
                badges[i] = TextureManager.instance.GetTexture("badge" + i);

            Floor = floor;
            lastLevel = Floor.Level;

            X = BrickCity.instance.Window.ClientBounds.Width / 20;
            Y = BrickCity.instance.Window.ClientBounds.Height - BrickCity.instance.Window.ClientBounds.Height / 20 - Height;
        }

        public Floor Floor
        {
            get
            {
                return floor;
            }
            set
            {
                floor = value;
                CreateFloors();
            }
        }

        private void CreateFloors()
        {
            RemoveAllComponents();

            for (int i = 0; i < 8; i++)
            {
                GuiTexture gt;
                int x = (24 + 12) * i;

                if (i + 1 == Floor.Level)
                    gt = new GuiTexture(this, x, 0, on);
                else if (i + 1 < Floor.Level)
                    gt = new GuiTexture(this, x, 0, complete);
                else
                    gt = new GuiTexture(this, x, 0, locked);

                gt.Width = gt.Height = 24;
                AddComponent(gt);

                //add badge
                if (i + 1 < Floor.Level)
                {
                    gt = new GuiTexture(this, x, 0, badges[i]);
                    gt.Width = gt.Height = 24;
                    AddComponent(gt);
                }
            }
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Visible) return;

            RenderBackground(spriteBatch);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null, Matrix.CreateTranslation(X + border, Y + border, 0));

            foreach (IGuiComponent igc in components)
                igc.Render(spriteBatch);

            spriteBatch.End();
        }
    }
}
