﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Text;

namespace BrickCity.Gui
{
    class GuiLabel : GuiComponent
    {
        public EAlignment HAlignment { get; set; }
        public EAlignment VAlignment { get; set; }
        public Color Color { get; set; }

        protected StringBuilder text = new StringBuilder("");
        private SpriteFont font;
        
        public GuiLabel(IGui gui, string text, int x, int y, EAlignment halign = EAlignment.LEFT, EAlignment valign = EAlignment.TOP) : base(gui, x, y)
        {
            Font = BrickCity.instance.Font;
            Text = text;
            HAlignment = halign;
            VAlignment = valign;
            Color = Color.White;

            CalcSize();
        }

        public SpriteFont Font
        {
            get { return font; }
            set
            {
                font = value;

                CalcSize();
            }
        }
        
        public override Rectangle TrueBounds
        {
            get
            {
                Rectangle bounds = base.TrueBounds;
                Vector2 size = Font.MeasureString(Text);

                switch (HAlignment)
                {
                    case EAlignment.CENTER:
                        bounds.X = (int)(bounds.X - size.X / 2);
                        break;
                    case EAlignment.RIGHT:
                        bounds.X = (int)(bounds.X - size.X);
                        break;
                }
                switch (VAlignment)
                {
                    case EAlignment.CENTER:
                        bounds.Y = (int)(bounds.Y - size.Y / 2);
                        break;
                    case EAlignment.BOTTOM:
                        bounds.Y = (int)(bounds.Y - size.Y);
                        break;
                }

                return bounds;
            }
        }

        public virtual string Text
        {
            get { return text.ToString(); }
            set
            {
                text.Clear();
                text.Append(value);
                CalcSize();
            }
        }

        protected virtual void CalcSize()
        {
            Vector2 size = Font.MeasureString(Text);
            Width = (int)size.X;
            Height = (int)size.Y;
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Enabled) return;

            Vector2 mod = Vector2.Zero;

            Vector2 size = Font.MeasureString(Text);

            switch (HAlignment)
            {
                case EAlignment.LEFT:
                    break;
                case EAlignment.CENTER:
                    mod.X = -size.X / 2;
                    break;
                case EAlignment.RIGHT:
                    mod.X = -size.X;
                    break;
            }
            switch (VAlignment)
            {
                case EAlignment.TOP:
                    break;
                case EAlignment.CENTER:
                    mod.Y = -size.Y / 2;
                    break;
                case EAlignment.BOTTOM:
                    mod.Y = -size.Y;
                    break;
            }

            spriteBatch.DrawString(Font, Text, Position + mod, Color);
        }
    }
}
