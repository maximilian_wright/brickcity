﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Gui
{
    class GuiButton : GuiLabel
    {
        public delegate void OnClick(int x, int y, int button);
        public event OnClick Clicked;

        public bool Hovered { get; private set; }

        private Texture2D[] textures;

        private enum EButtonState { NORMAL, HOVER, ACTIVE }
        private EButtonState buttonState = EButtonState.NORMAL;

        private Vector2 lastMousePos;

        public GuiButton(IGui gui, string text, Rectangle rect) : this(gui, text, rect.X, rect.Y, rect.Width, rect.Height) { }
        public GuiButton(IGui gui, string text, int x, int y, int w, int h) : base(gui, text, x, y, EAlignment.CENTER, EAlignment.CENTER)
        {
            Width = w;
            Height = h;

            textures = new Texture2D[3];
            textures[0] = TextureManager.instance.GetTexture("Gui/button");
            textures[1] = TextureManager.instance.GetTexture("Gui/buttonHover");
            textures[2] = TextureManager.instance.GetTexture("Gui/buttonActive");
        }

        public override Rectangle TrueBounds
        {
            get
            {
                Rectangle bounds = new Rectangle(Gui.X + X, Gui.Y + Y, Width, Height);

                switch (HAlignment)
                {
                    case EAlignment.CENTER:
                        bounds.X -= Width / 2;
                        break;
                    case EAlignment.RIGHT:
                        bounds.X -= Width;
                        break;
                }
                switch (VAlignment)
                {
                    case EAlignment.CENTER:
                        bounds.Y -= Height / 2;
                        break;
                    case EAlignment.BOTTOM:
                        bounds.Y -= Height;
                        break;
                }

                return bounds;
            }
        }

        protected override void CalcSize() { }

        public void SetButtonState(int state)
        {
            buttonState = (EButtonState)state;
        }

        public override void Update(float deltaTime)
        {
            if (!Enabled) return;

            Rectangle bounds = TrueBounds;
            
            if (bounds.Contains(lastMousePos) && !bounds.Contains(Input.MousePosition))
                OnMouseExit();
            else if (!bounds.Contains(lastMousePos) && bounds.Contains(Input.MousePosition))
                OnMouseEnter();
            else if (bounds.Contains(lastMousePos) && bounds.Contains(Input.MousePosition))
                OnMouseStay();

            lastMousePos = Input.MousePosition;

            if (Input.GetMouseUp(0) && Hovered)
                Click();
        }

        public void Click(int button = 0)
        {
            Clicked?.Invoke((int)Input.MousePosition.X, (int)Input.MousePosition.Y, button);
        }

        private void OnMouseEnter()
        {
            buttonState = EButtonState.HOVER;
            Hovered = true;
        }

        private void OnMouseStay()
        {
            if (Input.GetMouse(0))
                buttonState = EButtonState.ACTIVE;
            else
                buttonState = EButtonState.HOVER;

            Hovered = true;
        }

        private void OnMouseExit()
        {
            buttonState = EButtonState.NORMAL;
            Hovered = false;
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Enabled) return;

            Vector2 mod = Vector2.Zero;

            switch (HAlignment)
            {
                case EAlignment.CENTER:
                    mod.X = -Width / 2;
                    break;
                case EAlignment.RIGHT:
                    mod.X = -Width;
                    break;
            }
            switch (VAlignment)
            {
                case EAlignment.CENTER:
                    mod.Y = -Height / 2;
                    break;
                case EAlignment.BOTTOM:
                    mod.Y = -Height;
                    break;
            }

            spriteBatch.Draw(textures[(int)buttonState], new Rectangle((int)(X + mod.X), (int)(Y + mod.Y), Width, Height), Color.White);

            base.Render(spriteBatch);
        }
    }
}
