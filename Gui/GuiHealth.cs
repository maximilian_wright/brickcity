﻿using BrickCity.Entities;
using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Gui
{
    class GuiHealth : Gui
    {
        public const string NAME = "Health";

        private Player Player { get; set; }
        private Texture2D heart;
        private Texture2D halfHeart;
        private Texture2D noHeart;

        private int maxHealth;
        private int prevHealth;

        public GuiHealth(IState state, Player player, bool vis = true) : base(state, NAME, vis)
        {
            Player = player;
            maxHealth = player.MaxHealth;
            prevHealth = player.Health;

            X = Y = BrickCity.instance.Window.ClientBounds.Width / 20;
            
            heart = TextureManager.instance.GetTexture("health0");
            halfHeart = TextureManager.instance.GetTexture("health1");
            noHeart = TextureManager.instance.GetTexture("health2");

            CreateHearts();
        }

        private void CreateHearts()
        {
            RemoveAllComponents();

            for (int i = 0; i < Player.MaxHealth / 2; i++)
            {
                GuiTexture gt = new GuiTexture(this, (32 + 10) * i, 0, heart);
                gt.Width = gt.Height = 32;
                AddComponent(gt);
            }
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            if (maxHealth != Player.MaxHealth)
            {
                maxHealth = Player.MaxHealth;
                CreateHearts();
            }

            if (prevHealth != Player.Health)
            {
                prevHealth = Player.Health;
                
                for (int i = 0; i < prevHealth; i += 2)
                    ((GuiTexture)components[i / 2]).Texture = heart;

                if (prevHealth % 2 == 1)
                    ((GuiTexture)components[prevHealth / 2]).Texture = halfHeart;

                for (int i = prevHealth % 2 == 1 ? prevHealth + 1 : prevHealth; i < Player.MaxHealth; i += 2)
                    ((GuiTexture)components[i / 2]).Texture = noHeart;
            }
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            if (!Visible) return;

            RenderBackground(spriteBatch);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null, Matrix.CreateTranslation(X + border, Y + border, 0));

            foreach (IGuiComponent igc in components)
                igc.Render(spriteBatch);

            spriteBatch.End();
        }
    }
}
