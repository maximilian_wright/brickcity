﻿using BrickCity.State;
using Microsoft.Xna.Framework;
using System;

namespace BrickCity.Gui
{
    class GuiChar : Gui
    {
        public const string NAME = "Character";

        private GuiTextField nameField;
        private GuiButton playButton;

        public GuiChar(IState state, bool vis = true) : base(state, NAME, vis)
        {
            Rectangle size = BrickCity.instance.Window.ClientBounds;

            GuiLabel title = new GuiLabel(this, "Create Character", 0, 10, EAlignment.CENTER, EAlignment.CENTER);

            GuiLabel nameLabel = new GuiLabel(this, "Name:", 0, 50, EAlignment.LEFT, EAlignment.CENTER);
            nameField = new GuiTextField(this, 0, 50, 300, 50);

            GuiButton backButton = new GuiButton(this, "Back", 0, size.Height / 6, 200, 50);
            playButton = new GuiButton(this, "Play", 0, size.Height / 6, 200, 50);

            nameField.Submit += (text) =>
            {
                if (nameField.Text.Length > 0)
                    Play(nameField.Text.ToString());
            };

            backButton.Clicked += (mx, my, button) =>
            {
                state.ShowGui(GuiMainMenu.NAME);
                state.HideGui(NAME);
            };
            playButton.Clicked += (mx, my, button) =>
            {
                if (nameField.Text.Length > 0)
                    Play(nameField.Text.ToString());
            };

            AddComponent(title);

            AddComponent(nameLabel);
            AddComponent(nameField);

            AddComponent(backButton);
            AddComponent(playButton);

            Width = 700;

            X = size.Width / 2 - Width / 2;
            Y = size.Height / 2 - Height / 2;
            title.X = Width / 2;
            nameLabel.X = backButton.X = Width / 4;
            playButton.X = Width / 4 * 3;
            nameField.X = playButton.X - nameField.Width / 2;
        }

        private void Play(string name)
        {
            BrickCity.instance.CurrentState = new StateGame(name);
        }

        public override void Update(float deltaTime)
        {
            if (!Visible) return;

            base.Update(deltaTime);

            playButton.Enabled = nameField.Text.Length > 0;
        }
    }
}
