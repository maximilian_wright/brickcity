﻿namespace BrickCity.Gui
{
    enum EAlignment
    {
        LEFT, CENTER, RIGHT,
        TOP, BOTTOM
    }
}
