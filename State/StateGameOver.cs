﻿using BrickCity.Entities;
using BrickCity.Gui;
using Microsoft.Xna.Framework.Input;

namespace BrickCity.State
{
    class StateGameOver : State
    {
        private Player Player { get; set; }

        public StateGameOver(Player player) : base("GameOver")
        {
            Player = player;

            AddGui(new GuiGameOver(this, Player));
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            if (Input.GetKeyUp(Keys.Space))
            {
                BrickCity.instance.CurrentState = new StateMainMenu();
            }
        }
    }
}
