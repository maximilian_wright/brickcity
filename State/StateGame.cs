﻿using BrickCity.Gui;
using BrickCity.Map;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.State
{
    class StateGame : State
    {
        private Floor CurrentFloor { get; set; }

        private GuiMap map;
        private GuiFloors floors;
        private GuiNotification notify;

        public StateGame(string name) : base("Game")
        {
            CurrentFloor = new Floor(1);
            CurrentFloor.Generate(name);

            AddGui(new GuiHealth(this, CurrentFloor.CurrentRoom.Player));
            AddGui(map = new GuiMap(this, CurrentFloor));
            AddGui(floors = new GuiFloors(this, CurrentFloor));
            AddGui(notify = new GuiNotification(this));
            AddGui(new GuiPause(this, false));
        }

        public void NextFloor()
        {
            Floor newFloor = new Floor(CurrentFloor.Level + 1);
            newFloor.Generate(CurrentFloor.CurrentRoom.Player);

            CurrentFloor.OnExit();
            newFloor.OnEnter();

            map.Floor = newFloor;
            floors.Floor = newFloor;
            CurrentFloor = newFloor;
        }

        public void Notify(string text)
        {
            notify.Notify(text);
        }

        public override void Update(float deltaTime)
        {
            //update dungeon
            CurrentFloor.Update(deltaTime);

            //update guis
            base.Update(deltaTime);
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null, null);

            //draw floor
            CurrentFloor.Render(spriteBatch);

            spriteBatch.End();
        }
    }
}
