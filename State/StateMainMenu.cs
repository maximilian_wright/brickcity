﻿using BrickCity.Gui;
using Microsoft.Xna.Framework.Input;

namespace BrickCity.State
{
    class StateMainMenu : State
    {
        public StateMainMenu() : base("MainMenu")
        {
            AddGui(new GuiMainMenu(this));
            AddGui(new GuiChar(this, false));
            //AddGui(new GuiOptions(this, false));
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            if (Input.GetKeyUp(Keys.Escape))
                BrickCity.instance.Exit();
        }
    }
}
