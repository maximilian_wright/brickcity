﻿using BrickCity.Gui;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.State
{
    public interface IState
    {
        string Name { get; }

        void AddGui(IGui gui);
        void RemoveGui(IGui gui);
        void ShowGui(string name);
        void HideGui(string name);

        void Update(float deltaTime);
        void Render(SpriteBatch spriteBatch);
        void RenderGui(SpriteBatch spriteBatch);

        void OnTextInput(object sender, TextInputEventArgs tiea);
    }
}
