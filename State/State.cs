﻿using BrickCity.Gui;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace BrickCity.State
{
    abstract class State : IState
    {
        public string Name { get; private set; }

        protected Dictionary<string, IGui> guis;

        public State(string name)
        {
            Name = name;

            guis = new Dictionary<string, IGui>();
        }

        public void AddGui(IGui gui)
        {
            guis.Add(gui.Name, gui);
        }

        public void RemoveGui(IGui gui)
        {
            RemoveGui(gui.Name);
        }

        public void RemoveGui(string name)
        {
            guis.Remove(name);
        }

        public void ShowGui(string name)
        {
            if (!guis.ContainsKey(name))
                throw new System.Exception("Gui '" + name + "' does not exist.");

            guis[name].Show();
        }

        public void HideGui(string name)
        {
            if (!guis.ContainsKey(name))
                throw new System.Exception("Gui '" + name + "' does not exist.");

            guis[name].Hide();
        }

        public virtual void Update(float deltaTime)
        {
            foreach (KeyValuePair<string, IGui> pair in guis)
                pair.Value.Update(deltaTime);
        }

        public virtual void Render(SpriteBatch spriteBatch)
        {
            
        }

        public virtual void RenderGui(SpriteBatch spriteBatch)
        {
            foreach (KeyValuePair<string, IGui> pair in guis)
                pair.Value.Render(spriteBatch);
        }

        public virtual void OnTextInput(object sender, TextInputEventArgs tiea)
        {
            foreach (KeyValuePair<string, IGui> pair in guis)
                pair.Value.OnTextInput(sender, tiea);
        }
    }
}
