﻿namespace BrickCity.Entities
{
    class Treasure : Pickup
    {
        public ERarity Rarity { get; private set; }

        public Treasure(string name, ERarity rarity, string texture) : base(name, "Treasure/" + texture)
        {
            Important = true;
            Rarity = rarity;

            Width = Height = 32;

            PickedUp += OnTreasurePickedUp;
        }

        public virtual void OnTreasurePickedUp(IEntity e) { }

        public override string ToString()
        {
            return "Treasure[Name='" + Name + "',Rarity=" + Rarity.ToString() + "]";
        }

        /*
        * Treasure Names
        */
        public const string MAP = "I can see clearly now";
        public const string MAP_SECRET = "Secrets in the walls...";

        public const string BALLS1 = "Better Balls";
        public const string BALLS2 = "Betterer Balls";
        public const string BALLS3 = "Master Balls";

        public const string BIG_BALLS1 = "Check out those";
        public const string BIG_BALLS2 = "Isn't that too big?";
        public const string BIG_BALLS3 = "What are those!?";

        public const string SUCKY = "Sucky!";
    }

    /*
     * Treasure Classes
     */
    
    class TreasureBalls1 : Treasure
    {
        public TreasureBalls1() : base(BALLS1, ERarity.UNCOMMON, "upgrade0") { }

        public override void OnTreasurePickedUp(IEntity e)
        {
            Player p = (Player)e;

            p.BulletDamage += 1;
        }
    }

    class TreasureBalls2 : Treasure
    {
        public TreasureBalls2() : base(BALLS2, ERarity.UNCOMMON, "upgrade1") { }

        public override void OnTreasurePickedUp(IEntity e)
        {
            Player p = (Player)e;

            p.BulletDamage += 2;
        }
    }

    class TreasureBalls3 : Treasure
    {
        public TreasureBalls3() : base(BALLS3, ERarity.UNCOMMON, "upgrade2") { }

        public override void OnTreasurePickedUp(IEntity e)
        {
            Player p = (Player)e;

            p.BulletDamage += 3;
        }
    }

    class TreasureMap : Treasure
    {
        public TreasureMap() : base(MAP, ERarity.COMMON, "map") { }

        public override void OnTreasurePickedUp(IEntity e)
        {
            Player p = (Player)e;

            p.Room.Floor.Discover();
        }
    }

    class TreasureMapSecret : Treasure
    {
        public TreasureMapSecret() : base(MAP_SECRET, ERarity.COMMON, "mapSecret") { }

        public override void OnTreasurePickedUp(IEntity e)
        {
            Player p = (Player)e;

            p.Room.Floor.DiscoverSecret();
        }
    }

    class TreasureSucky : Treasure
    {
        public TreasureSucky() : base(SUCKY, ERarity.______, "sucky") { }
    }
}
