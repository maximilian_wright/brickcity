﻿using BrickCity.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Entities
{
    abstract class Entity : IEntity
    {
        public string Name { get; private set; }

        public float X { get; set; }
        public float Y { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        public bool Destroy { get; set; }

        public Room Room { get; set; }

        protected Sprite Sprite { get; private set; }

        public Entity(string name)
        {
            Name = name;

            Sprite = new Sprite();
        }

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle((int)X, (int)Y, (int)Width, (int)Height);
            }
        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(X, Y);
            }
        }

        public int TileX { get { return (int)(X / Tile.SIZE); } }
        public int TileY { get { return (int)(Y / Tile.SIZE); } }

        public virtual void Update(float deltaTime)
        {
            Sprite.Update(deltaTime);
        }

        public virtual void Render(SpriteBatch spriteBatch)
        {
            Sprite.Render(spriteBatch, Bounds);
        }
    }
}
