﻿using BrickCity.Map;
using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace BrickCity.Entities
{
    class Player : MovableEntity
    {
        private const int DEFAULT_MAX_HEALTH = 6;
        private const int DEFAULT_SIZE = 48;
        private const int DEFAULT_BULLET_SIZE = 16;
        private const int DEFAULT_BULLET_DAMAGE = 1;

        public delegate void OnRoomClear();
        public event OnRoomClear Cleared;

        public int Health { get; set; }

        public bool Up { get; private set; }
        public bool Down { get; private set; }
        public bool Left { get; private set; }
        public bool Right { get; private set; }

        private Dictionary<EStat, int> stats;

        private float attackTime = 0.35f;
        private float attackTimer = 0;
        private List<Keys> shootKeys;

        private List<Treasure> treasure;

        private Enemy lastDamage;

        public Player(string name) : base(name)
        {
            stats = new Dictionary<EStat, int>();
            stats.Add(EStat.MAX_HEALTH, DEFAULT_MAX_HEALTH);
            stats.Add(EStat.SIZE, DEFAULT_SIZE);
            stats.Add(EStat.BULLET_SIZE, DEFAULT_BULLET_SIZE);
            stats.Add(EStat.BULLET_DAMAGE, DEFAULT_BULLET_DAMAGE);

            Health = MaxHealth;
            Width = Height = Size;

            Speed = Tile.SIZE * 4;
            Sprite.AddImage("down", TextureManager.instance.GetTexture("player0"));
            Sprite.AddImage("left", TextureManager.instance.GetTexture("player1"));
            Sprite.AddImage("up", TextureManager.instance.GetTexture("player2"));
            Sprite.AddImage("right", TextureManager.instance.GetTexture("player3"));

            shootKeys = new List<Keys>();

            treasure = new List<Treasure>();
        }

        public int BulletDamage
        {
            get { return stats[EStat.BULLET_DAMAGE]; }
            set { stats[EStat.BULLET_DAMAGE] = value; }
        }

        public int BulletSize
        {
            get { return stats[EStat.BULLET_SIZE]; }
            set { stats[EStat.BULLET_SIZE] = value; }
        }

        public int MaxHealth
        {
            get { return stats[EStat.MAX_HEALTH]; }
            set { stats[EStat.MAX_HEALTH] = value; }
        }

        public int Size
        {
            get { return stats[EStat.SIZE]; }
            set { stats[EStat.SIZE] = value; }
        }

        public Enemy LastDamage
        {
            get { return lastDamage; }
        }

        private void Notify(string msg)
        {
            ((StateGame)BrickCity.instance.CurrentState).Notify(msg);
        }

        public void OnPickup(Pickup item)
        {
            if (item is Treasure)
            {
                Treasure t = (Treasure)item;
                treasure.Add(t);
                Notify(t.Name);
            }

            item.OnItemPickup(this);
            /*
            foreach (KeyValuePair<EStat, int> pair in item.Changes)
                switch (pair.Key)
                {
                    case EStat.HEALTH:
                        Health += pair.Value;

                        if (Health > MaxHealth) Health = MaxHealth;

                        break;
                    case EStat.SPEED:
                        Speed += pair.Value;

                        Notify("Speed " + (pair.Value > 0 ? "Up!" : "Down!"));
                        break;
                    default:
                        stats[pair.Key] += pair.Value;

                        string msg = "";

                        switch (pair.Key)
                        {
                            case EStat.BULLET_DAMAGE: msg += "Bullet Damage";
                                break;
                            case EStat.BULLET_SIZE: msg += "Bullet Size";
                                break;
                            case EStat.MAX_HEALTH: msg += "Max Health";
                                break;
                        }

                        Notify(msg + " " + (pair.Value > 0 ? "Up!" : "Down!"));
                        break;
                }*/
        }

        public void OnDamage(Enemy enemy, int damage)
        {
            lastDamage = enemy;
            Health -= damage;
        }

        public override void Update(float deltaTime)
        {
            attackTimer -= deltaTime;
            Left = Right = Up = Down = false;
            Direction = new Vector2(0, 0);

            if (Input.GetKeyUp(Keys.H))
                Health--;

            if (Health <= 0)
            {
                BrickCity.instance.CurrentState = new StateGameOver(this);
                return;
            }

            KeyboardState state = Keyboard.GetState();

            if (Input.GetKey(Keys.W))
            {
                Up = true;
                Direction = new Vector2(Direction.X, -1);
            }
            if (Input.GetKey(Keys.R))
            {
                Down = true;
                Direction = new Vector2(Direction.X, 1);
            }
            if (Input.GetKey(Keys.A))
            {
                Left = true;
                Direction = new Vector2(-1, Direction.Y);
            }
            if (Input.GetKey(Keys.S))
            {
                Right = true;
                Direction = new Vector2(1, Direction.Y);
            }

            X += Direction.X * Speed * deltaTime;
            Y += Direction.Y * Speed * deltaTime;

            if (Input.GetKeyDown(Keys.Down))
                Push(Keys.Down);
            if (Input.GetKeyDown(Keys.Up))
                Push(Keys.Up);
            if (Input.GetKeyDown(Keys.Left))
                Push(Keys.Left);
            if (Input.GetKeyDown(Keys.Right))
                Push(Keys.Right);

            if (Input.GetKeyUp(Keys.Down))
                Pop(Keys.Down);
            if (Input.GetKeyUp(Keys.Up))
                Pop(Keys.Up);
            if (Input.GetKeyUp(Keys.Left))
                Pop(Keys.Left);
            if (Input.GetKeyUp(Keys.Right))
                Pop(Keys.Right);

            if (shootKeys.Count == 0)
            {
                Sprite.Show("down");
                return;
            }

            switch (shootKeys[shootKeys.Count - 1])
            {
                case Keys.Down:
                    Sprite.Show("down");

                    if (attackTimer <= 0)
                        Shoot(0);
                    break;
                case Keys.Up:
                    Sprite.Show("up");

                    if (attackTimer <= 0)
                        Shoot(2);
                    break;
                case Keys.Left:
                    Sprite.Show("left");

                    if (attackTimer <= 0)
                        Shoot(1);
                    break;
                case Keys.Right:
                    Sprite.Show("right");

                    if (attackTimer <= 0)
                        Shoot(3);
                    break;
            }
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            base.Render(spriteBatch);

            if (BrickCity.instance.Debug)
            {
                int mod = BrickCity.instance.Font.LineSpacing;
                int y = -mod;
                spriteBatch.DrawString(BrickCity.instance.Font, "Floor: " + Room.Floor.Level, new Vector2(0, y += mod), Color.Yellow);
                spriteBatch.DrawString(BrickCity.instance.Font, "Room: (" + Room.X + ", " + Room.Y + ")", new Vector2(0, y += mod), Color.Yellow);
                spriteBatch.DrawString(BrickCity.instance.Font, "Position: (" + X + ", " + Y + ")", new Vector2(0, y += mod), Color.Yellow);
                spriteBatch.DrawString(BrickCity.instance.Font, "Tile: (" + TileX + ", " + TileY + ")", new Vector2(0, y += mod), Color.Yellow);
                spriteBatch.DrawString(BrickCity.instance.Font, "Direction: (" + Direction.X + ", " + Direction.Y + ")", new Vector2(0, y += mod), Color.Yellow);
                spriteBatch.DrawString(BrickCity.instance.Font, "Speed: " + Speed, new Vector2(0, y += mod), Color.Yellow);
                spriteBatch.DrawString(BrickCity.instance.Font, "Health: " + Health + " / " + MaxHealth, new Vector2(0, y += mod), Color.Yellow);

                DrawRect(spriteBatch, Bounds, Color.White);
            }
        }

        private void DrawRect(SpriteBatch spriteBatch, Rectangle rect, Color color)
        {
            Texture2D b = TextureManager.instance.GetTexture("base");
            spriteBatch.Draw(b, new Rectangle(rect.X, rect.Y, rect.Width, 1), color);
            spriteBatch.Draw(b, new Rectangle(rect.X, rect.Bottom - 1, rect.Width, 1), color);
            spriteBatch.Draw(b, new Rectangle(rect.X, rect.Y, 1, rect.Height), color);
            spriteBatch.Draw(b, new Rectangle(rect.Right - 1, rect.Y, 1, rect.Height), color);
        }

        private void Push(Keys key)
        {
            if (shootKeys.Contains(key))
                shootKeys.Remove(key);
            
            shootKeys.Add(key);
        }

        private void Pop(Keys key)
        {
            if (shootKeys.Contains(key))
                shootKeys.Remove(key);
        }

        private void Shoot(int dir)
        {
            attackTimer = attackTime;

            Bullet b = new Bullet(X + Width / 2, Y + Height / 2, BulletSize, BulletDamage);

            switch (dir)
            {
                case 0:
                    b.Direction = Vector2.UnitY;
                    break;
                case 1:
                    b.Direction = -Vector2.UnitX;
                    break;
                case 2:
                    b.Direction = -Vector2.UnitY;
                    break;
                case 3:
                    b.Direction = Vector2.UnitX;
                    break;
            }

            Room.AddEntity(b);
        }

        public void OnClear()
        {
            Cleared?.Invoke();
        }
    }
}
