﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace BrickCity.Entities
{
    class Sprite
    {
        protected Dictionary<string, Tuple<float, List<Texture2D>>> frames;

        private string currentAnimation = null;
        private int currentIndex = 0;

        private bool Static { get; set; }
        private bool Playing { get; set; }
        private float frameTimer = 0;

        public Sprite(bool playing = false)
        {
            Playing = playing;
            frames = new Dictionary<string, Tuple<float, List<Texture2D>>>();
            Static = true;
        }

        public Sprite(string name, Texture2D image, bool playing = false)
        {
            Playing = playing;
            frames = new Dictionary<string, Tuple<float, List<Texture2D>>>();
            frames.Add(name, Tuple.Create(-1.0f, new List<Texture2D>()));
            frames[name].Item2.Add(image);
            Static = true;

            currentAnimation = name;
        }

        public Sprite(string name, float frameTime, Texture2D[] images, bool playing = true)
        {
            Playing = playing;
            frames = new Dictionary<string, Tuple<float, List<Texture2D>>>();
            frames.Add(name, Tuple.Create(frameTime, new List<Texture2D>()));
            Static = false;

            for (int i = 0; i < images.Length; i++)
                frames[name].Item2.Add(images[i]);

            currentAnimation = name;
        }

        public string[] Animations
        {
            get
            {
                List<string> animations = new List<string>();

                foreach (string anim in frames.Keys)
                    animations.Add(anim);

                return animations.ToArray();
            }
        }

        private int NumAnimations
        {
            get
            {
                return frames.Count;
            }
        }

        private int NumFrames
        {
            get
            {
                return frames[currentAnimation].Item2.Count;
            }
        }

        public void AddImage(string name, Texture2D image)
        {
            if (frames.ContainsKey(name))
            {
                throw new Exception("Animation '" + name + "' already added.");
                return;
            }

            if (NumAnimations == 0)
                currentAnimation = name;

            frames.Add(name, Tuple.Create(-1.0f, new List<Texture2D>()));
            frames[name].Item2.Add(image);
        }

        public void AddAnimation(string name, float frameTime, Texture2D[] images)
        {
            if (frames.ContainsKey(name))
            {
                throw new Exception("Animation '" + name + "' already added.");
                return;
            }
            
            Static = false;

            if (NumAnimations == 0)
                currentAnimation = name;

            frames.Add(name, Tuple.Create(frameTime, new List<Texture2D>()));

            for (int i = 0; i < images.Length; i++)
                frames[name].Item2.Add(images[i]);
        }

        public void SetImage(string name, Texture2D image)
        {
            if (!frames.ContainsKey(name))
            {
                AddImage(name, image);
                return;
            }

            frames[name].Item2.Clear();
            frames[name].Item2.Add(image);
        }

        public bool RemoveAnimation(string name)
        {
            return frames.Remove(name);
        }

        public bool Play(string name)
        {
            if (!frames.ContainsKey(name)) return false;

            Playing = true;
            currentAnimation = name;
            currentIndex = 0;
            frameTimer = 0;

            return true;
        }

        public void Pause()
        {
            Playing = false;
        }

        public bool Show(string name)
        {
            if (!frames.ContainsKey(name)) return false;

            currentAnimation = name;

            return true;
        }

        public void Stop()
        {
            Playing = false;
            currentIndex = 0;
        }

        public void Update(float deltaTime)
        {
            if (Playing)
            {
                frameTimer += deltaTime;

                if (frameTimer >= frames[currentAnimation].Item1)
                {
                    currentIndex = (currentIndex + 1) % NumFrames;
                    frameTimer = 0;
                }
            }
        }

        public virtual void Render(SpriteBatch spriteBatch, Rectangle bounds)
        {
            spriteBatch.Draw(frames[currentAnimation].Item2[currentIndex], bounds, Color.White);
        }

        public virtual void Render(SpriteBatch spriteBatch, Rectangle bounds, Color color)
        {
            spriteBatch.Draw(frames[currentAnimation].Item2[currentIndex], bounds, color);
        }
    }
}
