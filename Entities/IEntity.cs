﻿using BrickCity.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Entities
{
    interface IEntity
    {
        Vector2 Position { get; }
        float X { get; set; }
        float Y { get; set; }
        float Width { get; set; }
        float Height { get; set; }
        Rectangle Bounds { get; }

        int TileX { get; }
        int TileY { get; }

        bool Destroy { get; set; }

        Room Room { get; set; }

        void Update(float deltaTime);
        void Render(SpriteBatch spriteBatch);
    }
}
