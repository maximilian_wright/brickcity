﻿namespace BrickCity.Entities
{
    enum EStat
    {
        BULLET_DAMAGE, BULLET_SIZE, HEALTH, MAX_HEALTH, SPEED, SIZE
    }
}
