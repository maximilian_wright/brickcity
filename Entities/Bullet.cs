﻿using BrickCity.Map;
using Microsoft.Xna.Framework;

namespace BrickCity.Entities
{
    class Bullet : MovableEntity
    {
        public int Damage { get; set; }
        public bool Piercing { get; set; }

        public Bullet(float x, float y, int size = 16, int damage = 0, bool pierce = false, float speed = Tile.SIZE * 6) : base("Andrew Scialla")
        {
            X = x - size / 2;
            Y = y - size / 2;
            Damage = damage;
            Piercing = pierce;
            Speed = speed;
            Direction = Vector2.Zero;
            Width = Height = size;

            Sprite.AddImage("bullet0", TextureManager.instance.GetTexture("bullet0"));
            Sprite.AddImage("bullet1", TextureManager.instance.GetTexture("bullet1"));
            Sprite.AddImage("bullet2", TextureManager.instance.GetTexture("bullet2"));
            Sprite.AddImage("bullet3", TextureManager.instance.GetTexture("bullet3"));

            if (Damage >= 5)
                Sprite.Play("bullet3");
            else if (Damage >= 3)
                Sprite.Play("bullet2");
            else if (Damage > 1)
                Sprite.Play("bullet1");
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            X += Direction.X * Speed * deltaTime;
            Y += Direction.Y * Speed * deltaTime;

            if (X < -Width ||
                X > Room.PixelWidth ||
                Y < -Height ||
                Y > Room.PixelHeight)
                Destroy = true;
        }
    }
}
