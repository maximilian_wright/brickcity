﻿using BrickCity.Map;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace BrickCity.Entities
{
    class Pickup : Entity
    {
        public delegate void OnPickup(IEntity entity);
        public event OnPickup PickedUp;

        public bool Important { get; set; }
        public Dictionary<EStat, int> Changes { get; private set; }

        public Pickup(string name, string texture) : base(name)
        {
            Changes = new Dictionary<EStat, int>();

            Sprite.AddImage("default", TextureManager.instance.GetTexture(texture));
        }

        public void AddChange(EStat stat, int mod)
        {
            Changes.Add(stat, mod);
        }

        public void AddChanges(Dictionary<EStat, int> delta)
        {
            foreach (KeyValuePair<EStat, int> pair in delta)
                AddChange(pair.Key, pair.Value);
        }

        public void OnItemPickup(IEntity entity)
        {
            PickedUp?.Invoke(entity);
        }

        /// <summary>
        /// Called to spawn hearts and other regular drops
        /// </summary>
        /// <param name="random"></param>
        /// <returns></returns>
        public static List<Pickup> SpawnRegulars(Random random, Room room)
        {
            List<Pickup> pickups = new List<Pickup>();
            Vector2 tile = GetValidSpawnTile(room);

            //health
            double hpChance = 0.65;
            double halfChance = hpChance / 3;
            double num;
            
            while ((num = random.NextDouble()) < hpChance)
            {
                PickupHealth ph = new PickupHealth(num < halfChance ? 1 : 2);
                ph.X = tile.X;
                ph.Y = tile.Y;
                pickups.Add(ph);

                hpChance /= 2;
            }

            return pickups;
        }

        private static Vector2 GetValidSpawnTile(Room room)
        {
            int x = Room.WIDTH / 2;
            int y = Room.HEIGHT / 2;

            if (!room.Tiles[x, y].Collidable)
                return new Vector2(x, y);

            int dis = 0;

            while (x > 0 && y > 0)
            {
                x--; y--; dis += 2;

                for (int xx = 0; xx < dis; xx++)
                {
                    if (x + xx < 0 || x + xx >= Room.WIDTH)
                        continue;
                    
                    if (!room.Tiles[x + xx, y].Collidable)
                        return new Vector2(x + xx, y);
                    else if (!room.Tiles[x + xx, y + dis].Collidable)
                        return new Vector2(x + xx, y + dis);
                }

                for (int yy = 0; yy < dis; yy++)
                {
                    if (y + yy < 0 || y + yy >= Room.WIDTH)
                        continue;
                    
                    if (!room.Tiles[x, y + yy].Collidable)
                        return new Vector2(x, y + yy);
                    else if (!room.Tiles[x + dis, y + yy].Collidable)
                        return new Vector2(x + dis, y + yy);
                }
            }

            return new Vector2(-1, -1);
        }
    }

    class PickupHealth : Pickup
    {
        public PickupHealth(int heal = 2) : base("Heart", heal == 2 ? "health0" : "health1")
        {
            Important = true;

            bool dirty = false;

            if (heal < 1) { heal = 1; dirty = true; }
            if (heal > 2) { heal = 2; dirty = true; }

            AddChange(EStat.HEALTH, heal);

            if (dirty)
                Sprite.SetImage("default", TextureManager.instance.GetTexture(heal == 2 ? "health0" : "health1"));
        }
    }
}
