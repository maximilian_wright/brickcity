﻿using BrickCity.Map;
using System;
using System.Collections.Generic;

namespace BrickCity.Entities
{
    class Boss : Enemy
    {
        private static string[] NAMES = new string[]
        {
            "THE DERK OF ERRKESs",
            "CAVE HERPES",
            "WATERMAN",
            "TEAM MAGMA",
            "BRICK CITY I SHRUNK RITCHIE"
        };

        public int Difficulty { get; set; }
        
        private Random random;
        private bool lootDropped = false;

        private enum EManeuver
        {
            CHASE, CHARGE, SHOOT, TELEPORT, HEAL
        }
        private List<EManeuver> maneuvers;
        private EManeuver currentManeuver;

        public Boss(Random random, int difficulty, int nameIndex, int hp) : base(NAMES[nameIndex], hp, hp)
        {
            this.random = random;
            Difficulty = difficulty;
            maneuvers = new List<EManeuver>();
            Width = Height = 196;
            X = Room.WIDTH * Tile.SIZE / 2 - Width / 2;
            Y = Room.HEIGHT * Tile.SIZE / 2 - Height / 2;

            EManeuver[] values = (EManeuver[])Enum.GetValues(typeof(EManeuver));

            for (int i = 0; i < difficulty; i++)
                maneuvers.Add(values[random.Next(0, values.Length)]);

            switch (nameIndex)
            {
                case 0:
                    Sprite.AddAnimation("idle", 0.4f, TextureManager.instance.GetBossTextures(new string[]
                        { "thederkoferrkess0", "thederkoferrkess1" }));
                    break;
                case 1:
                    Sprite.AddAnimation("idle", 0.4f, TextureManager.instance.GetBossTextures(new string[]
                        { "herpes0", "herpes1" }));
                    break;
                case 2:
                    Sprite.AddAnimation("idle", 0.4f, TextureManager.instance.GetBossTextures(new string[]
                        { "waterman0", "waterman1" }));
                    break;
                case 3:
                    Sprite.AddAnimation("idle", 0.4f, TextureManager.instance.GetBossTextures(new string[]
                        { "teammagma0", "teammagma1" }));
                    break;
                case 4:
                    Sprite.AddImage("idle", TextureManager.instance.GetBoss("ritchie"));
                    break;
            }

            Sprite.Play("idle");
            NextManeuver();
        }

        private void NextManeuver()
        {
            currentManeuver = maneuvers[random.Next(0, maneuvers.Count)];
        }

        public override void Update(float deltaTime)
        {
            if (IsDead)
            {
                if (!lootDropped)
                {
                    lootDropped = true;

                    ((BossRoom)Room).UnlockDoor();
                    SpawnExit();
                    DropTreasure();
                }

                return;
            }

            base.Update(deltaTime);

            switch (currentManeuver)
            {
                case EManeuver.CHASE:
                    break;
                case EManeuver.CHARGE:
                    break;
                case EManeuver.SHOOT:
                    break;
                case EManeuver.HEAL:
                    break;
                case EManeuver.TELEPORT:
                    break;
            }
        }

        private void SpawnExit()
        {
            Room.SetTile(Room.WIDTH / 2, 3, new TileExitFloor(Room, Room.WIDTH / 2, 3, EDirection.NONE));
        }

        private void DropTreasure()
        {
            double value = random.NextDouble();
            Treasure t;

            if (value < 0.01)
            {
                t = new TreasureSucky();
            }
            else if (value < 0.1)
            {
                t = TreasureManager.instance.GetTreasure(ERarity.______);
            }
            else if (value < 0.25)
            {
                t = TreasureManager.instance.GetTreasure(ERarity.SECRET);
            }
            else if (value < 0.45)
            {
                t = TreasureManager.instance.GetTreasure(ERarity.SUPER_RARE);
            }
            else
            {
                t = TreasureManager.instance.GetTreasure(ERarity.RARE);
            }

            t.X = Room.WIDTH / 2 * Tile.SIZE;
            t.Y = (Room.HEIGHT - 3) * Tile.SIZE;
            Room.AddEntity(t);

            List<Pickup> pickups = Pickup.SpawnRegulars(random, Room);
            foreach (Pickup p in pickups)
                Room.AddEntity(p);
        }

        public override string ToString()
        {
            return "Boss[Name='" + Name + "',Difficulty=" + Difficulty + ",MaxHP=" + MaxHealth + "]";
        }

        public static Boss Generate(Random random, int difficulty)
        {
            return new Boss(random, difficulty, random.Next(0, NAMES.Length), random.Next(100 + difficulty * 75, 200 + difficulty * 55));
        }
    }
}
