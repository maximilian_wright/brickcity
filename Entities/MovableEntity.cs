﻿using Microsoft.Xna.Framework;

namespace BrickCity.Entities
{
    class MovableEntity : Entity
    {
        public float Speed { get; set; }

        private Vector2 direction;

        public MovableEntity(string name) : base(name) { }

        public Vector2 Direction
        {
            get { return direction; }
            set
            {
                direction = value;

                if (direction != Vector2.Zero)
                    direction.Normalize();
            }
        }
    }
}
