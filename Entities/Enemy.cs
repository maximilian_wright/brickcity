﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BrickCity.Entities
{
    class Enemy : MovableEntity
    {
        public int Health { get; set; }
        public int MaxHealth { get; set; }

        protected Texture2D barTexture;

        public Enemy(string name, int hp, int maxHp) : base(name)
        {
            Health = hp;
            MaxHealth = maxHp;

            barTexture = TextureManager.instance.GetTexture("base");
        }

        public bool IsDead
        {
            get { return Health <= 0; }
        }

        public void ApplyDamage(int dmg)
        {
            Health -= dmg;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            Health = Clamp(Health, 0, MaxHealth);

            if (IsDead) return;
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            //hp bar
            Rectangle barBounds = new Rectangle((int)X, (int)(Y - 20), (int)Width, 20);
            Rectangle hpBounds = new Rectangle((int)(X + 3), (int)(Y - 17), (int)((Width - 6) * ((float)Health / MaxHealth)), 14);
            spriteBatch.Draw(barTexture, barBounds, Color.White);
            spriteBatch.Draw(barTexture, hpBounds, Color.Black);

            if (IsDead)
            {
                Vector2 size = BrickCity.instance.Font.MeasureString("DEAD");
                spriteBatch.DrawString(BrickCity.instance.Font, "DEAD",
                    new Vector2(barBounds.X + (barBounds.Width - size.X) / 2, barBounds.Y + (barBounds.Height - size.Y) / 2),
                    Color.Red);
            }

            //sprite
            Sprite.Render(spriteBatch, Bounds, IsDead ? Color.Navy : Color.White);
        }

        private int Clamp(int value, int min, int max)
        {
            if (value < min) return min;
            else if (value > max) return max;

            return value;
        }
    }
}
