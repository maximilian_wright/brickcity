﻿using BrickCity.Entities;
using System;

namespace BrickCity.Map
{
    class TreasureRoom : Room
    {
        public TreasureRoom(Floor floor, int x, int y) : base(floor, x, y) { }

        public void Init(EDirection from, Room neighbor)
        {
            //create secret exit
            switch (from)
            {
                case EDirection.DOWN:
                    tiles[WIDTH / 2, 0] = new TileExit(this, WIDTH / 2, 0, EDirection.UP);
                    neighbor.Bottom = this;
                    neighbor.SetTile(WIDTH / 2, HEIGHT - 1, new TileExitSecret(neighbor, WIDTH / 2, HEIGHT - 1, EDirection.DOWN));
                    break;
                case EDirection.LEFT:
                    tiles[WIDTH - 1, HEIGHT / 2] = new TileExit(this, WIDTH - 1, HEIGHT / 2, EDirection.RIGHT);
                    neighbor.Left = this;
                    neighbor.SetTile(0, HEIGHT / 2, new TileExitSecret(neighbor, 0, HEIGHT / 2, EDirection.LEFT));
                    break;
                case EDirection.UP:
                    tiles[WIDTH / 2, HEIGHT - 1] = new TileExit(this, WIDTH / 2, HEIGHT - 1, EDirection.DOWN);
                    neighbor.Top = this;
                    neighbor.SetTile(WIDTH / 2, 0, new TileExitSecret(neighbor, WIDTH / 2, 0, EDirection.UP));
                    break;
                case EDirection.RIGHT:
                    tiles[0, HEIGHT / 2] = new TileExit(this, 0, HEIGHT / 2, EDirection.LEFT);
                    neighbor.Right = this;
                    neighbor.SetTile(WIDTH - 1, HEIGHT / 2, new TileExitSecret(neighbor, WIDTH - 1, HEIGHT / 2, EDirection.RIGHT));
                    break;
            }

            GenerateTreasure();
        }

        private void GenerateTreasure()
        {
            if (Floor.Level == 1)
            {
                Pickup p = TreasureManager.instance.GetTreasure(ERarity.UNCOMMON);
                Log.WriteLine(p);
                p.X = WIDTH / 2 * Tile.SIZE + p.Width / 2;
                p.Y = HEIGHT / 2 * Tile.SIZE + p.Height / 2;
                AddPickup(p);
            }
        }

        public override string ToString()
        {
            return "TreasureRoom[X=" + X + ",Y=" + Y + "]";
        }
    }
}
