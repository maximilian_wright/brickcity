﻿using System;

namespace BrickCity.Map
{
    [Flags]
    enum EDirection
    {
        NONE = 0,
        DOWN = 1,
        LEFT = 2,
        UP = 4,
        RIGHT = 8
    }
}
