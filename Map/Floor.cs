﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BrickCity.Map
{
    class Floor
    {
        public const int MIN_ROOMS = 5;
        public const int MAX_ROOMS = 12;

        public int Level { get; private set; }
        public EFloorType Type { get; private set; }

        private Dictionary<Tuple<int, int>, Room> rooms;
        public Room CurrentRoom { get; private set; }

        private Random random;

        public Floor(int level = 0, EFloorType type = EFloorType.STONE)
        {
            Level = level;
            Type = type;

            rooms = new Dictionary<Tuple<int, int>, Room>();

            random = new Random();
        }

        public int NumRooms
        {
            get
            {
                return rooms.Count;
            }
        }

        public Dictionary<Tuple<int, int>, Room> Rooms
        {
            get { return rooms; }
        }

        public void Generate(object player)
        {
            AddRoom(CurrentRoom = new Room(this).CreateStart(player));

            while (!CurrentRoom.Generate(random))
            {
                rooms.Clear();
                AddRoom(CurrentRoom);
            }

            GenerateTreasure();
            GenerateBoss();

            foreach (KeyValuePair<Tuple<int, int>, Room> pair in rooms)
                Log.WriteLine(pair.Value);
        }

        private void GenerateTreasure()
        {
            IEnumerable<Room> choice = Utils.RandomValues(rooms).Take(1);
            Room nearRoom = choice.ElementAt(0);

            while (!HasRoom(nearRoom))
            {
                choice = Utils.RandomValues(rooms).Take(1);
                nearRoom = choice.ElementAt(0);
            }

            Tuple<int, int> key = null;
            EDirection dir = EDirection.NONE;

            if (nearRoom.Bottom == null)
            {
                key = nearRoom.KeyDown;
                dir = EDirection.DOWN;
            }
            else if (nearRoom.Left == null)
            {
                key = nearRoom.KeyLeft;
                dir = EDirection.LEFT;
            }
            else if (nearRoom.Top == null)
            {
                key = nearRoom.KeyUp;
                dir = EDirection.UP;
            }
            else if (nearRoom.Right == null)
            {
                key = nearRoom.KeyRight;
                dir = EDirection.RIGHT;
            }

            TreasureRoom treasureRoom = new TreasureRoom(this, key.Item1, key.Item2);
            treasureRoom.Create();
            treasureRoom.Init(dir, nearRoom);
            AddRoom(treasureRoom);
        }

        private bool HasRoom(Room room)
        {
            return room.Top == null || room.Bottom == null || room.Left == null || room.Right == null;
        }

        private void GenerateBoss()
        {
            IEnumerable<Room> choice = Utils.RandomValues(rooms).Take(1);
            Room nearRoom = choice.ElementAt(0);

            while (!HasRoom2(nearRoom))
            {
                choice = Utils.RandomValues(rooms).Take(1);
                nearRoom = choice.ElementAt(0);
            }

            Tuple<int, int> key = null;
            EDirection dir = EDirection.NONE;

            if (nearRoom.Bottom == null)
            {
                key = nearRoom.KeyDown;
                dir = EDirection.DOWN;
            }
            else if (nearRoom.Left == null)
            {
                key = nearRoom.KeyLeft;
                dir = EDirection.LEFT;
            }
            else if (nearRoom.Top == null)
            {
                key = nearRoom.KeyUp;
                dir = EDirection.UP;
            }
            else if (nearRoom.Right == null)
            {
                key = nearRoom.KeyRight;
                dir = EDirection.RIGHT;
            }

            BossRoom bossRoom = new BossRoom(this, key.Item1, key.Item2);
            bossRoom.Create();
            bossRoom.Init(dir, nearRoom);
            AddRoom(bossRoom);
        }

        /// <summary>
        /// Checks if location is valid for Boss
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        private bool HasRoom2(Room room)
        {
            return !(room is TreasureRoom) &&
                ((room.Top == null && !(room.Top is TreasureRoom)) ||
                (room.Bottom == null && !(room.Bottom is TreasureRoom)) ||
                (room.Left == null && !(room.Left is TreasureRoom)) ||
                (room.Right == null && !(room.Right is TreasureRoom)));
        }

        public void AddRoom(Room room)
        {
            //assign this room's neighbors
            if (rooms.ContainsKey(room.KeyDown))
                room.Bottom = rooms[room.KeyDown];
            if (rooms.ContainsKey(room.KeyLeft))
                room.Left = rooms[room.KeyLeft];
            if (rooms.ContainsKey(room.KeyUp))
                room.Top = rooms[room.KeyUp];
            if (rooms.ContainsKey(room.KeyRight))
                room.Right = rooms[room.KeyRight];

            //TODO - fix room key collisions (spawning multiple rooms at certain coordinates)
            //maybe leave as a feature?
            if (!rooms.ContainsKey(room.Key))
                rooms.Add(room.Key, room);
        }

        //makes all rooms (except secret) Discovered
        public void Discover()
        {
            foreach (KeyValuePair<Tuple<int, int>, Room> pair in rooms)
                if (!(pair.Value is TreasureRoom))
                    pair.Value.Discovered = true;
        }

        public void DiscoverSecret()
        {
            foreach (KeyValuePair<Tuple<int, int>, Room> pair in rooms)
                if (pair.Value is TreasureRoom)
                {
                    pair.Value.Discovered = true;
                    return;
                }
        }

        public virtual void OnEnter()
        {

        }

        public virtual void OnExit()
        {

        }

        public void ChangeRoom(Room newRoom)
        {
            CurrentRoom.OnLeave();

            EDirection dir = CurrentRoom.GetDirection(newRoom);

            switch (dir)
            {
                case EDirection.DOWN:
                    CurrentRoom.Player.Y = CurrentRoom.Player.Y % Tile.SIZE;

                    if (newRoom is BossRoom) CurrentRoom.Player.Y += Tile.SIZE;
                    break;
                case EDirection.LEFT:
                    CurrentRoom.Player.X = (Room.WIDTH - 1) * Tile.SIZE + (Tile.SIZE - CurrentRoom.Player.Width) / 2;

                    if (newRoom is BossRoom) CurrentRoom.Player.X -= Tile.SIZE;
                    break;
                case EDirection.UP:
                    CurrentRoom.Player.Y = (Room.HEIGHT - 1) * Tile.SIZE + (Tile.SIZE - CurrentRoom.Player.Height) / 2;

                    if (newRoom is BossRoom) CurrentRoom.Player.Y -= Tile.SIZE;
                    break;
                case EDirection.RIGHT:
                    CurrentRoom.Player.X = CurrentRoom.Player.X % Tile.SIZE;

                    if (newRoom is BossRoom) CurrentRoom.Player.X += Tile.SIZE;
                    break;
                default:
                    break;
            }

            newRoom.AddEntity(CurrentRoom.Player);
            newRoom.OnEnter();

            CurrentRoom = newRoom;
        }

        public void Update(float deltaTime)
        {
            CurrentRoom.Update(deltaTime);
        }

        public void Render(SpriteBatch spriteBatch)
        {
            CurrentRoom.Render(spriteBatch);
        }
    }
}
