﻿using BrickCity.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace BrickCity.Map
{
    class Room
    {
        public const int WIDTH = 13;
        public const int HEIGHT = 11;

        public int X { get; private set; }
        public int Y { get; private set; }

        public Room Left { get; set; }
        public Room Right { get; set; }
        public Room Top { get; set; }
        public Room Bottom { get; set; }

        public bool Discovered { get; set; }

        public int NumEnemies { get; private set; }
        public bool Cleared { get; set; }

        public Player Player { get; private set; }

        public Floor Floor { get; private set; }

        private List<IEntity> entities;
        private List<Enemy> enemies;
        private List<Pickup> pickups;
        protected Tile[,] tiles;

        public Room(Floor floor) : this(floor, 0, 0) { }
        public Room(Floor floor, int x, int y)
        {
            Cleared = false;
            Discovered = false;

            Floor = floor;
            Left = Right = Top = Bottom = null;
            X = x;
            Y = y;
            entities = new List<IEntity>();
            enemies = new List<Enemy>();
            pickups = new List<Pickup>();
            tiles = new Tile[WIDTH, HEIGHT];
        }

        public Tuple<int, int> Key { get { return new Tuple<int, int>(X, Y); } }
        public Tuple<int, int> KeyDown { get { return new Tuple<int, int>(X, Y + 1); } }
        public Tuple<int, int> KeyLeft { get { return new Tuple<int, int>(X - 1, Y); } }
        public Tuple<int, int> KeyUp { get { return new Tuple<int, int>(X, Y - 1); } }
        public Tuple<int, int> KeyRight { get { return new Tuple<int, int>(X + 1, Y); } }

        public Tile[,] Tiles { get { return tiles; } }

        public Tile TileDown { get { return tiles[WIDTH / 2, HEIGHT - 1]; } }
        public Tile TileLeft { get { return tiles[0, HEIGHT / 2]; } }
        public Tile TileUp { get { return tiles[WIDTH / 2, 0]; } }
        public Tile TileRight { get { return tiles[WIDTH - 1, HEIGHT / 2]; } }

        public EDirection Exits
        {
            get
            {
                EDirection exits = EDirection.NONE;

                if (TileDown.GetType().ToString().Contains("Exit") &&
                    !TileDown.GetType().ToString().Contains("Secret") &&
                    Bottom != null)
                    exits |= EDirection.DOWN;
                if (TileUp.GetType().ToString().Contains("Exit") &&
                    !TileDown.GetType().ToString().Contains("Secret") &&
                    Top != null)
                    exits |= EDirection.UP;
                if (TileLeft.GetType().ToString().Contains("Exit") &&
                    !TileDown.GetType().ToString().Contains("Secret") &&
                    Left != null)
                    exits |= EDirection.LEFT;
                if (TileRight.GetType().ToString().Contains("Exit") &&
                    !TileDown.GetType().ToString().Contains("Secret") &&
                    Right != null)
                    exits |= EDirection.RIGHT;

                return exits;
            }
        }

        public int PixelWidth { get { return WIDTH * Tile.SIZE; } }
        public int PixelHeight { get { return HEIGHT * Tile.SIZE; } }

        public EDirection GetDirection(Room dest)
        {
            if (Top == dest)
                return EDirection.UP;
            else if (Bottom == dest)
                return EDirection.DOWN;
            else if (Left == dest)
                return EDirection.LEFT;
            else if (Right == dest)
                return EDirection.RIGHT;

            return EDirection.NONE;
        }

        public Room CreateStart(object player)
        {
            Discovered = true;

            if (player is string)
            {
                string name = (string)player;
                Player p = new Player(name);
                p.X = WIDTH * Tile.SIZE / 2 - p.Width / 2;
                p.Y = HEIGHT * Tile.SIZE / 2 - p.Height / 2;
                AddEntity(p);
            }
            else
                AddEntity((Player)player);


            CreateDefault();

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rand"></param>
        /// <returns>Whether or not to regen the floor. if successfully
        /// created Floor.MIN_ROOMS</returns>
        public bool Generate(Random rand)
        {
            EDirection exits = EDirection.NONE;

            if (rand.NextDouble() < 0.5)
                exits |= EDirection.DOWN;
            if (rand.NextDouble() < 0.5)
                exits |= EDirection.LEFT;
            if (rand.NextDouble() < 0.5)
                exits |= EDirection.UP;
            if (rand.NextDouble() < 0.5)
                exits |= EDirection.RIGHT;

            if (Bottom == null && exits.HasFlag(EDirection.DOWN) && Floor.NumRooms < Floor.MAX_ROOMS)
                CreateBottom(rand);
            if (Left == null && exits.HasFlag(EDirection.LEFT) && Floor.NumRooms < Floor.MAX_ROOMS)
                CreateLeft(rand);
            if (Top == null && exits.HasFlag(EDirection.UP) && Floor.NumRooms < Floor.MAX_ROOMS)
                CreateTop(rand);
            if (Right == null && exits.HasFlag(EDirection.RIGHT) && Floor.NumRooms < Floor.MAX_ROOMS)
                CreateRight(rand);

            if (Floor.NumRooms < Floor.MIN_ROOMS)
                return false;

            return true;
        }

        private void CreateDefault()
        {
            for (int i = 0; i < WIDTH; i++)
            {
                tiles[i, 0] = new Tile(this, Tile.STONE_WALL, i, 0);
                tiles[i, HEIGHT - 1] = new Tile(this, Tile.STONE_WALL, i, HEIGHT - 1);
            }

            for (int i = 0; i < HEIGHT; i++)
            {
                tiles[0, i] = new Tile(this, Tile.STONE_WALL, 0, i);
                tiles[WIDTH - 1, i] = new Tile(this, Tile.STONE_WALL, WIDTH - 1, i);
            }

            for (int i = 1; i < WIDTH - 1; i++)
                for (int k = 1; k < HEIGHT - 1; k++)
                    tiles[i, k] = new Tile(this, Tile.STONE, i, k);
        }

        public Room Create()
        {
            Log.WriteLine("Create(" + this + ")");

            CreateDefault();

            return this;
        }

        private void CreateTop(Random rand)
        {
            if (Top != null) return;

            CreateExit(EDirection.UP);
            Top = new Room(Floor, X, Y - 1);
            Floor.AddRoom(Top.Create());
            Top.Generate(rand);
            Top.CreateExit(EDirection.DOWN);
        }

        private void CreateBottom(Random rand)
        {
            if (Bottom != null) return;

            CreateExit(EDirection.DOWN);
            Bottom = new Room(Floor, X, Y + 1);
            Floor.AddRoom(Bottom.Create());
            Bottom.Generate(rand);
            Bottom.CreateExit(EDirection.UP);
        }

        private void CreateLeft(Random rand)
        {
            if (Left != null) return;

            CreateExit(EDirection.LEFT);
            Left = new Room(Floor, X - 1, Y);
            Floor.AddRoom(Left.Create());
            Left.Generate(rand);
            Left.CreateExit(EDirection.RIGHT);
        }

        private void CreateRight(Random rand)
        {
            if (Right != null) return;

            CreateExit(EDirection.RIGHT);
            Right = new Room(Floor, X + 1, Y);
            Floor.AddRoom(Right.Create());
            Right.Generate(rand);
            Right.CreateExit(EDirection.LEFT);
        }

        /// <summary>
        /// Places an exit tile for a direction
        /// </summary>
        /// <param name="dir"></param>
        private void CreateExit(EDirection dir)
        {
            switch (dir)
            {
                case EDirection.DOWN:
                    tiles[WIDTH / 2, HEIGHT - 1] = new TileExit(this, WIDTH / 2, HEIGHT - 1, EDirection.DOWN);
                    break;
                case EDirection.LEFT:
                    tiles[0, HEIGHT / 2] = new TileExit(this, 0, HEIGHT / 2, EDirection.LEFT);
                    break;
                case EDirection.UP:
                    tiles[WIDTH / 2, 0] = new TileExit(this, WIDTH / 2, 0, EDirection.UP);
                    break;
                case EDirection.RIGHT:
                    tiles[WIDTH - 1, HEIGHT / 2] = new TileExit(this, WIDTH - 1, HEIGHT / 2, EDirection.RIGHT);
                    break;
            }
        }

        /// <summary>
        /// Called to spawn an entity into this Room
        /// </summary>
        /// <param name="e"></param>
        public void AddEntity(IEntity e)
        {
            e.Room = this;
            entities.Add(e);

            if (e is Player)
                Player = (Player)e;
            else if (e is Enemy)
                NumEnemies++;
        }

        /// <summary>
        /// Add an Enemy to the list of spawnables
        /// </summary>
        /// <param name="e"></param>
        public void AddEnemy(Enemy e)
        {
            enemies.Add(e);
        }

        /// <summary>
        /// Add a pickup to the list of spawnables
        /// </summary>
        /// <param name="p"></param>
        public void AddPickup(Pickup p)
        {
            pickups.Add(p);
        }

        public void SetTile(int x, int y, Tile tile)
        {
            if (x < 0 || x >= WIDTH ||
                y < 0 || y >= HEIGHT)
                throw new Exception("Out of bounds: " + x + ", " + y);

            tiles[x, y] = tile;
        }

        public void Update(float deltaTime)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].Destroy)
                {
                    if (entities[i] is Enemy)
                    {
                        NumEnemies--;

                        if (NumEnemies == 0)
                            Cleared = true;
                    }

                    entities.RemoveAt(i);
                    i--;
                    continue;
                }

                entities[i].Update(deltaTime);
            }

            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int k = 0; k < tiles.GetLength(1); k++)
                {
                    tiles[i, k].Update(deltaTime);

                    //if we changed rooms
                    if (Floor.CurrentRoom != this)
                        return;
                }

            //temp lists
            List<Enemy> es = new List<Enemy>();
            List<Bullet> bs = new List<Bullet>();
            List<Pickup> ps = new List<Pickup>();

            foreach (IEntity e in entities)
                if (e is Enemy)
                    es.Add((Enemy)e);
                else if (e is Bullet)
                    bs.Add((Bullet)e);
                else if (e is Pickup)
                    ps.Add((Pickup)e);

            if (es.Count == 0)
                OnClear();

            //"physics"
            /*
            * Player
            */
            foreach (Pickup p in ps)
                if (p.Bounds.Intersects(Player.Bounds) || p.Bounds.Contains(Player.Bounds))
                {
                    Player.OnPickup(p);
                    p.Destroy = true;
                }

            if (Player.X < 0) Player.X = 0;
            if (Player.X >= PixelWidth - Player.Width) Player.X = PixelWidth - Player.Width;
            if (Player.Y < 0) Player.Y = 0;
            if (Player.Y >= PixelHeight - Player.Height) Player.Y = PixelHeight - Player.Height;

            int px = Player.TileX;
            int py = Player.TileY;

            for (int y = -1; y <= 1; y++)
            {
                for (int x = -1; x <= 1; x++)
                {
                    if ((px + x < 0 || px + x >= WIDTH) ||
                        (py + y < 0 || py + y >= HEIGHT))
                        continue;

                    Tile t = tiles[px + x, py + y];

                    if (!t.Collidable) continue;
                    
                    if (t.Bounds.Intersects(Player.Bounds))
                    {
                        Vector2 col;

                        switch (GetPlayerDirection())
                        {
                            case 1://down
                                Player.Y = t.Bounds.Y - Player.Height;
                                break;
                            case 2://left
                                Player.X = t.Bounds.Right;
                                break;
                            case 3://up
                                Player.Y = t.Bounds.Bottom;
                                break;
                            case 4://right
                                Player.X = t.Bounds.X - Player.Width;
                                break;
                            case 5://down left
                                col = new Vector2(t.Bounds.Right - Player.Bounds.Left, Player.Bounds.Bottom - t.Bounds.Top);

                                if (col.X == col.Y)
                                {
                                    Tile right = tiles[px + x + 1, py + y];
                                    Tile top = tiles[px + x, py + y - 1];

                                    if (top.Collidable && !right.Collidable)
                                        col = new Vector2(t.Bounds.Right - Player.Bounds.Left, 0);
                                    else if (!top.Collidable && right.Collidable)
                                        col = new Vector2(0, Player.Bounds.Bottom - t.Bounds.Top);
                                }

                                CorrectPlayer(col, false, true);
                                break;
                            case 6://left up
                                col = new Vector2(t.Bounds.Right - Player.Bounds.Left, t.Bounds.Bottom - Player.Bounds.Top);

                                if (col.X == col.Y)
                                {
                                    Tile right = tiles[px + x + 1, py + y];
                                    Tile bot = tiles[px + x, py + y + 1];

                                    if (bot.Collidable && !right.Collidable)
                                        col = new Vector2(t.Bounds.Right - Player.Bounds.Left, 0);
                                    else if(!bot.Collidable && right.Collidable)
                                        col = new Vector2(0, t.Bounds.Bottom - Player.Bounds.Top);
                                }

                                CorrectPlayer(col, false, false);
                                break;
                            case 7://right up
                                col = new Vector2(Player.Bounds.Right - t.Bounds.Left, t.Bounds.Bottom - Player.Bounds.Top);

                                if (col.X == col.Y)
                                {
                                    Tile left = tiles[px + x - 1, py + y];
                                    Tile bot = tiles[px + x, py + y + 1];

                                    if (bot.Collidable && !left.Collidable)
                                        col = new Vector2(Player.Bounds.Right - t.Bounds.Left, 0);
                                    else if (!bot.Collidable && left.Collidable)
                                        col = new Vector2(0, t.Bounds.Bottom - Player.Bounds.Top);
                                }

                                CorrectPlayer(col, true, false);
                                break;
                            case 8://down right
                                col = new Vector2(Player.Bounds.Right - t.Bounds.Left, Player.Bounds.Bottom - t.Bounds.Top);
                                CorrectPlayer(col, true, true);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            /*
            * Bullet
            */
            foreach (Bullet b in bs)
            {
                int bx = b.TileX;
                int by = b.TileY;

                foreach (Enemy e in es)
                {
                    if (e.IsDead) continue;

                    if (e.Bounds.Intersects(b.Bounds) || e.Bounds.Contains(b.Bounds))
                    {
                        e.ApplyDamage(b.Damage);

                        if (!b.Piercing)
                            b.Destroy = true;
                    }
                }

                for (int x = -1; x <= 1; x++)
                    for (int y = -1; y <= 1; y++)
                    {
                        if ((bx + x < 0 || bx + x >= WIDTH) ||
                        (by + y < 0 || by + y >= HEIGHT))
                            continue;

                        Tile t = tiles[bx + x, by + y];

                        if (b.Piercing || !t.Collidable) continue;

                        if (t.Bounds.Intersects(b.Bounds))
                            b.Destroy = true;
                    }
            }
        }

        private void CorrectPlayer(Vector2 col, bool flipx, bool flipy)
        {
            if (col.X == col.Y)
            {
                Player.X += col.X * (flipx ? -1 : 1);
                Player.Y += col.Y * (flipy ? -1 : 1);
            }
            else if (col.X < col.Y)
                Player.X += col.X * (flipx ? -1 : 1);
            else
                Player.Y += col.Y * (flipy ? -1 : 1);
        }

        private int GetPlayerDirection()
        {
            Vector2 dir = Player.Direction;

            if (dir.X == 0 && dir.Y == 1) return 1;
            else if (dir.X == -1 && dir.Y == 0) return 2;
            else if (dir.X == 0 && dir.Y == -1) return 3;
            else if (dir.X == 1 && dir.Y == 0) return 4;

            else if (dir.X < 0 && dir.Y > 0) return 5;
            else if (dir.X < 0 && dir.Y < 0) return 6;
            else if (dir.X > 0 && dir.Y < 0) return 7;
            else if (dir.X > 0 && dir.Y > 0) return 8;

            return 0;
        }

        public void Render(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
                for (int k = 0; k < tiles.GetLength(1); k++)
                    tiles[i, k].Render(spriteBatch);

            foreach (IEntity e in entities)
                e.Render(spriteBatch);
        }

        private void SpawnEnemies()
        {
            while (enemies.Count > 0)
            {
                Enemy e = enemies[0];
                enemies.RemoveAt(0);
                AddEntity(e);
            }
        }

        private void SpawnPickups()
        {
            while (pickups.Count > 0)
            {
                Pickup p = pickups[0];
                pickups.RemoveAt(0);
                AddEntity(p);
            }
        }

        public virtual void OnClear()
        {
            Cleared = true;
            Player.OnClear();
        }

        public virtual void OnEnter()
        {
            Discovered = true;

            SpawnPickups();

            if (!Cleared)
                SpawnEnemies();

            tiles[Player.TileX, Player.TileY].Place(Player);
        }

        public void OnLeave()
        {
            foreach (IEntity e in entities)
            {
                if (e is Enemy)
                    AddEnemy((Enemy)e);
                else if (e is Pickup)
                {
                    Pickup p = (Pickup)e;

                    if (p.Important)
                        AddPickup(p);
                }
            }

            entities.Clear();
        }

        public override string ToString()
        {
            return "Room[X=" + X + ",Y=" + Y + "]";
        }
    }
}
