﻿using BrickCity.Entities;

namespace BrickCity.Map
{
    class BossRoom : Room
    {
        private Tile exitTile;
        private Boss boss;

        public BossRoom(Floor floor, int x, int y) : base(floor, x, y)
        {

        }

        public void Init(EDirection from, Room neighbor)
        {
            //create exit
            switch (from)
            {
                case EDirection.DOWN:
                    tiles[WIDTH / 2, 0] = new TileExitBoss(this, WIDTH / 2, 0, EDirection.UP);
                    exitTile = tiles[WIDTH / 2, 0];
                    neighbor.Bottom = this;
                    neighbor.SetTile(WIDTH / 2, HEIGHT - 1, new TileExitBoss(neighbor, WIDTH / 2, HEIGHT - 1, EDirection.DOWN));
                    break;
                case EDirection.LEFT:
                    tiles[WIDTH - 1, HEIGHT / 2] = new TileExitBoss(this, WIDTH - 1, HEIGHT / 2, EDirection.RIGHT);
                    exitTile = tiles[WIDTH - 1, HEIGHT / 2];
                    neighbor.Left = this;
                    neighbor.SetTile(0, HEIGHT / 2, new TileExitBoss(neighbor, 0, HEIGHT / 2, EDirection.LEFT));
                    break;
                case EDirection.UP:
                    tiles[WIDTH / 2, HEIGHT - 1] = new TileExitBoss(this, WIDTH / 2, HEIGHT - 1, EDirection.DOWN);
                    exitTile = tiles[WIDTH / 2, HEIGHT - 1];
                    neighbor.Top = this;
                    neighbor.SetTile(WIDTH / 2, 0, new TileExitBoss(neighbor, WIDTH / 2, 0, EDirection.UP));
                    break;
                case EDirection.RIGHT:
                    tiles[0, HEIGHT / 2] = new TileExitBoss(this, 0, HEIGHT / 2, EDirection.LEFT);
                    exitTile = tiles[0, HEIGHT / 2];
                    neighbor.Right = this;
                    neighbor.SetTile(WIDTH - 1, HEIGHT / 2, new TileExitBoss(neighbor, WIDTH - 1, HEIGHT / 2, EDirection.RIGHT));
                    break;
            }

            GenerateBoss();
        }

        public void GenerateBoss()
        {
            boss = Boss.Generate(new System.Random(), Floor.Level);
            AddEnemy(boss);
        }

        public override void OnEnter()
        {
            base.OnEnter();

            if (!Cleared)
                LockDoor();
        }

        private void LockDoor()
        {
            exitTile.Collidable = true;
        }

        public void UnlockDoor()
        {
            exitTile.Collidable = false;
        }

        public override string ToString()
        {
            return "BossRoom[X=" + X + ",Y=" + Y + ",Boss=" + boss + "]";
        }
    }
}
