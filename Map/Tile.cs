﻿using BrickCity.Entities;
using BrickCity.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace BrickCity.Map
{
    class Tile
    {
        public const int SIZE = 64;

        public int ID { get; set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public bool Collidable { get; set; }

        private Room Room { get; set; }

        private bool contains = false;

        public Tile(Room room, int id, int x, int y)
        {
            ID = id;
            X = x;
            Y = y;
            Collidable = ID > 100;

            Room = room;
        }

        public Rectangle Bounds
        {
            get { return new Rectangle(X * SIZE, Y * SIZE, SIZE, SIZE); }
        }

        /// <summary>
        /// Called when an entity is placed on this tile
        /// </summary>
        /// <param name="e"></param>
        public void Place(IEntity e)
        {
            contains = true;
        }

        public virtual void Update(float deltaTime)
        {
            if (Bounds.Contains(Room.Player.Bounds))
            {
                if (!contains)
                {
                    if (!OnStep(Room.Player))
                        contains = true;
                }
            }
            else
            {
                if (contains)
                    OnStepOut(Room.Player);

                contains = false;
            }
        }

        public void Render(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(TextureManager.instance.GetTile(ID), new Rectangle(X * SIZE, Y * SIZE, SIZE, SIZE), Color.White);
        }

        /// <summary>
        /// Called when an entity steps into this tile
        /// </summary>
        /// <param name="e"></param>
        public virtual bool OnStep(IEntity e)
        {
            return false;
        }

        /// <summary>
        /// Called when an entity steps out of this tile
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnStepOut(IEntity e)
        {

        }

        public override string ToString()
        {
            return "Tile[ID=" + ID + ",X=" + X + ",Y=" + Y + "]";
        }

        public const int EXIT = 1;
        public const int STONE = 2;
        public const int STONE_SECRET = 3;

        public const int BOSS = 50;
        public const int FLOOR_EXIT = 51;
        
        //unpassable
        public const int STONE_WALL = 101;
    }

    class TileExit : Tile
    {
        private EDirection direction;

        public TileExit(Room room, int x, int y, EDirection dir) : this(room, EXIT, x, y, dir) { }
        protected TileExit(Room room, int id, int x, int y, EDirection dir) : base(room, id, x, y)
        {
            direction = dir;
        }

        public override bool OnStep(IEntity e)
        {
            if (e is Player)
            {
                switch (direction)
                {
                    case EDirection.DOWN:
                        e.Room.Floor.ChangeRoom(e.Room.Bottom);
                        break;
                    case EDirection.LEFT:
                        e.Room.Floor.ChangeRoom(e.Room.Left);
                        break;
                    case EDirection.UP:
                        e.Room.Floor.ChangeRoom(e.Room.Top);
                        break;
                    case EDirection.RIGHT:
                        e.Room.Floor.ChangeRoom(e.Room.Right);
                        break;
                    default:
                        throw new Exception("Unknown exit direction: " + direction);
                }
            }

            return true;
        }
    }

    class TileExitBoss : TileExit
    {
        public TileExitBoss(Room room, int x, int y, EDirection dir) : base(room, BOSS, x, y, dir) { }
    }

    class TileExitSecret : TileExit
    {
        public TileExitSecret(Room room, int x, int y, EDirection dir) : base(room, STONE_SECRET, x, y, dir) { }
    }

    class TileExitFloor : TileExit
    {
        public TileExitFloor(Room room, int x, int y, EDirection dir) : base(room, FLOOR_EXIT, x, y, dir) { }

        public override bool OnStep(IEntity e)
        {
            if (e is Player)
                ((StateGame)BrickCity.instance.CurrentState).NextFloor();

            return true;
        }
    }
}
