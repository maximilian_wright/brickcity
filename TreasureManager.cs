﻿using BrickCity.Entities;
using System;
using System.Collections.Generic;

namespace BrickCity
{
    class TreasureManager
    {
        public static TreasureManager instance;

        private Random random;
        private Dictionary<ERarity, List<Treasure>> treasure;

        static TreasureManager()
        {
            if (instance == null)
                instance = new TreasureManager();
        }

        private TreasureManager()
        {
            random = new Random();
            treasure = new Dictionary<ERarity, List<Treasure>>();

            foreach (ERarity rarity in Enum.GetValues(typeof(ERarity)))
                treasure.Add(rarity, new List<Treasure>());

            #region Common
            Add(new TreasureMap());
            Add(new TreasureMapSecret());
            #endregion

            #region Uncommon
            Add(new TreasureBalls1());
            Add(new TreasureBalls2());
            Add(new TreasureBalls3());
            #endregion

            #region Rare
            #endregion

            #region Super Rare
            #endregion

            #region Secret
            #endregion

            #region ______
            #endregion
        }

        private void Add(Treasure t)
        {
            treasure[t.Rarity].Add(t);
        }

        public Treasure GetTreasure(ERarity rarity)
        {
            if (treasure[rarity].Count == 0)
                return new TreasureSucky();

            return treasure[rarity][random.Next(0, treasure[rarity].Count)];
        }
    }
}
