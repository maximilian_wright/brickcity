﻿using System;
namespace BrickCity
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                new BrickCity().Run();
            }
            catch (Exception e)
            {
                Log.WriteErr(e);
                Log.Close();
            }
        }
    }
#endif
}
