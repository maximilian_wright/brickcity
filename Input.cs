﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BrickCity
{
    class Input
    {
        private static KeyboardState curKeyState;
        private static KeyboardState prevKeyState;
        private static MouseState curMouseState;
        private static MouseState prevMouseState;

        public static bool GetKeyDown(Keys key)
        {
            return curKeyState[key] == KeyState.Down && prevKeyState[key] == KeyState.Up;
        }

        public static bool GetKeyUp(Keys key)
        {
            return curKeyState[key] == KeyState.Up && prevKeyState[key] == KeyState.Down;
        }

        public static bool GetKey(Keys key)
        {
            return curKeyState[key] == KeyState.Down;
        }

        public static bool GetMouseDown(int button)
        {
            switch (button)
            {
                case 0: return curMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released;
                case 1: return curMouseState.RightButton == ButtonState.Pressed && prevMouseState.RightButton == ButtonState.Released;
                case 2: return curMouseState.MiddleButton == ButtonState.Pressed && prevMouseState.MiddleButton == ButtonState.Released;
                default: return false;
            }
        }

        public static bool GetMouseUp(int button)
        {
            switch (button)
            {
                case 0: return curMouseState.LeftButton == ButtonState.Released && prevMouseState.LeftButton == ButtonState.Pressed;
                case 1: return curMouseState.RightButton == ButtonState.Released && prevMouseState.RightButton == ButtonState.Pressed;
                case 2: return curMouseState.MiddleButton == ButtonState.Released && prevMouseState.MiddleButton == ButtonState.Pressed;
                default: return false;
            }
        }

        public static bool GetMouse(int button)
        {
            switch (button)
            {
                case 0: return curMouseState.LeftButton == ButtonState.Pressed;
                case 1: return curMouseState.RightButton == ButtonState.Pressed;
                case 2: return curMouseState.MiddleButton == ButtonState.Pressed;
                default: return false;
            }
        }

        public static bool DidMouseMove()
        {
            return curMouseState.Position != prevMouseState.Position;
        }

        public static Vector2 MousePosition
        {
            get { return new Vector2(curMouseState.X, curMouseState.Y); }
        }

        public static void Update()
        {
            prevKeyState = curKeyState;
            prevMouseState = curMouseState;
            curKeyState = Keyboard.GetState();
            curMouseState = Mouse.GetState();
        }
    }
}
